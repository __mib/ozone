!include EnvVarUpdate.nsh
Name "O3 Installer"
OutFile "install.exe"

InstallDir $DESKTOP\O3
RequestExecutionLevel user

Page directory
Page instfiles

;--------------------------------

; The stuff to install
Section "Python 2.7" ;No components page, name is not important

  SetOutPath $INSTDIR
  File python-2.7.msi
  ExecWait '"msiexec" /i "$INSTDIR\python-2.7.msi"'
  Delete python-2.7.msi

; EnvVar
  ${EnvVarUpdate} $0 "PATH" "A" "HKLM" "C:\Python27"
  
  File /r "ogrepy"
  SetOutPath "$INSTDIR\ogrepy\"
  ExecWait '"C:\Python27\python.exe" "$INSTDIR\ogrepy\setup.py" install'  
  SetOutPath $INSTDIR
  ExecWait "$INSTDIR\ogrepy\tools\oalinst.exe"
  File dxwebsetup.exe
  ExecWait dxwebsetup.exe
  Delete dxwebsetup.exe
  RMDir /r "ogrepy"
  File /r "game"
  File "Run.ico"
  SetOutPath "$INSTDIR\game\environ"
  CreateShortCut "$DESKTOP\AwesomeGame.lnk" "$INSTDIR\game\environ\run.bat" "$INSTDIR\Run.ico" "$INSTDIR\Run.ico"

SectionEnd ; end the section
