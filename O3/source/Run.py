import sys, os
sys.path.append('../source')
sys.path.append('../source/game')
sys.path.append('../source/levels')
sys.path.append('../source/actors')
os.environ['PYTHONPATH'] = ""
if __name__ == '__main__':
    from game.Main import Application
    app = Application()
    app.run()
