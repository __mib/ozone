import game.Level
import ogre.renderer.OGRE as Ogre
from game.CharacterSkillControl import CharacterSkillControl
#@ActorFactory

class SampleLevel(game.Level.Level):

    
    def onLevelLoaded(self):
        print "post-load"
        print "Adding "
        self.hero = self.actorForName("SampleHero")
        m = self.sceneManager.getRootSceneNode().getChildIterator()
        while m.hasMoreElements() :
            print(m.getNext().getName())
        
        heroNode = self.sceneManager.getRootSceneNode().getChild("SampleHero")    
        heroActor = self.actorForNode(heroNode)
        print "node, actor", heroNode, heroActor
        self.cameraMan.setTarget(heroNode)
        self.charControl.setCharacter(heroActor)
        
        heroActor.setUsableSkills(heroActor.getAllSkills())
        skill_control = CharacterSkillControl(self.getTimeEventManager(), self.getMainIOListener().getKeyboard(), heroActor)
        #self.sceneManager.setFog(Ogre.FOG_LINEAR, Ogre.ColourValue(0.1, 0.1, 0.1), 0.0, 30, 120);

    #def registerCallbacks(self, mGenericCharacterControl):
    #    mainCharacter = self.actorForName("Hero")
    #    mGenericCharacterControl.setCharacter(mainCharacter)
    
    _settings = {
                'selectorAvailable': True,
                'name': "SampleLevel",
                'skyPlane': 'SunSkyPlane',
                'playerActorName': "SampleHero",
    }
    
    def getSettings(self):
        return self._settings
