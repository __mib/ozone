import game.Level
import ogre.renderer.OGRE as Ogre
from game.CharacterSkillControl import CharacterSkillControl
from actors.Mobs import GhostActivator
#@ActorFactory
def tShootingMsg(self, actor2):
    if hasattr(actor2, '_pc_marker'): 
        if self.used: return
        self.used = True
        self.getLevel().getGUI().addDialogLine("Overlord", "Use your mouse to destroy certain objects.")
def tFloorWarnMsg(self, actor2):
    if hasattr(actor2, '_pc_marker'): 
        if self.used: return
        self.used = True
        if hasattr(actor2, '_pc_marker'): self.getLevel().getGUI().addDialogLine("Overlord", "The floor ahead will damage you.")
def tHealMsg(self, actor2):
    if hasattr(actor2, '_pc_marker'): 
        if self.used: return
        self.used = True
        if hasattr(actor2, '_pc_marker'): self.getLevel().getGUI().addDialogLine("Overlord", "Press (H) to heal yourself.")

class TutorialLevel(game.Level.Level):

    def setupTutorialMessages(self):
        self.actorForName('TShooting').used = False
        self.actorForName('TDFloorWarn').used = False
        self.actorForName('THeal').used = False
        
        instancemethod = type (GhostActivator.collisionEvent)
        self.actorForName('TShooting').collisionEvent = instancemethod(tShootingMsg, self.actorForName('TShooting'), GhostActivator)
        self.actorForName('TDFloorWarn').collisionEvent = instancemethod(tFloorWarnMsg, self.actorForName('TDFloorWarn'), GhostActivator)        
        self.actorForName('THeal').collisionEvent = instancemethod(tHealMsg, self.actorForName('THeal'), GhostActivator)
        self.getGUI().addDialogLine("Overlord", "(W)(A)(S)(D) to move, (SPACE) to jump.")
    
    def onLevelLoaded(self):
        self.getGameState().addCompositor("Old Movie")
        
        self.hero = self.actorForName("SampleHero")
        m = self.sceneManager.getRootSceneNode().getChildIterator()
        while m.hasMoreElements() :
            print(m.getNext().getName())
        
        heroNode = self.sceneManager.getRootSceneNode().getChild("SampleHero")    
        heroActor = self.actorForNode(heroNode)

        self.cameraMan.setTarget(heroNode)
        self.charControl.setCharacter(heroActor)
        
        heroActor.setUsableSkills(heroActor.getAllSkills())
        skill_control = CharacterSkillControl(self.getTimeEventManager(), self.getMainIOListener().getKeyboard(), heroActor)
        #self.sceneManager.setFog(Ogre.FOG_LINEAR, Ogre.ColourValue(0.1, 0.1, 0.1), 0.0, 30, 120);
    
        plane = Ogre.Plane((0, -1, 1), -5000)
        self.getSceneManager().setSkyPlane(True, plane, 'TutorialSky', 160, 1)
        
        node = self.sceneManager.getRootSceneNode().getChild("MovingDoorYellow")
        print "node", node
        door_body = self.factory.bodyForNode( "MovingDoorYellow" )
        print "db ", door_body
        door_slider = self.factory.getSlider(door_body)
        print "slider", door_slider
        door_slider.setTargetLinMotorVelocity(-1.0)
        
        self.getSoundManager().addSoundToNode(heroActor.getNode(), "spawn.ogg", loop=False)
        #self.getSoundManager().addSoundToNode(heroActor.getNode(), "tack.mp3", loop=False)
        self.getSoundManager().addSoundToNode(heroActor.getNode(), "flight_sound.ogg", loop=False)
        self.getSoundManager().addSoundToNode(heroActor.getNode(), "OutThere.ogg", loop=True)
        self.setupTutorialMessages()
        

    
    _settings = {
                'selectorAvailable': True,
                'name': "SampleLevel",
                #'skyPlane': 'TutorialSky',
                'playerActorName': "SampleHero",
    }
    
    def getSettings(self):
        return self._settings
