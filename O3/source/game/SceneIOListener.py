import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS

class SecondaryIOListener(object):
    
    def mouseMoved(self, evt):
        return True
 
    def mousePressed(self, evt, bid):
        return True
 
    def mouseReleased(self, evt, bid):
        return True 
 
    def keyPressed(self, evt):
        return True
 
    def keyReleased(self, evt):
        return True

    def destroyAll(self):
        print "WARNING: generic destroyAll on object", self
        print "WARNING cont'd: this could lead to memory leaks and duplicate scene instances"

class SceneIOListener(OIS.MouseListener, OIS.KeyListener):

    def __init__(self, mouse, keyboard, log):
        OIS.MouseListener.__init__(self)
        OIS.KeyListener.__init__(self)
        self.mouse = mouse
        self.keyboard = keyboard
        self.depressed = set()
        self.secondary = set()
    
    def getKeyboard(self):
        return self.keyboard
    
    def registerSecondary(self, listener):
        #if not isinstance(listener, SecondaryIOListener):
        #    self.log.logMessage("Attempt to register non-SecondaryIOListener object", Ogre.LML_CRITICAL)
        #    return False
        self.secondary.add(listener)
        return True
    
    def destroyAll(self):
        for x in self.secondary:
            x.destroyAll()
        del self.secondary
    
    def unregisterSecondary(self, listener):
        self.secondary.remove(listener)
    
    def isKeyPressed(self, key):
        return key in self.depressed
        
    def focus(self):
        self.mouse.setEventCallback(self)
        self.keyboard.setEventCallback(self)
     
    def defocus(self):
        self.mouse.setEventCallback(None)
        self.keyboard.setEventCallback(None)
        self.depressed.clear()
        
    def mouseMoved(self, evt):
        for listener in self.secondary:
            listener.mouseMoved(evt)
        return True
 
    def mousePressed(self, evt, bid):
        print "mousepressed"
        for listener in self.secondary:
            listener.mousePressed(evt, bid)
        return True
 
    def mouseReleased(self, evt, bid):
        print "mousereleased"
        for listener in self.secondary:
            listener.mouseReleased(evt, bid)
        return True 
 
    def keyPressed(self, evt):
        self.depressed.add(evt.key)
        for listener in self.secondary:
            listener.keyPressed(evt)
        return True
 
    def keyReleased(self, evt):
        if evt.key in self.depressed:
            self.depressed.remove(evt.key)
        for listener in self.secondary:
            listener.keyReleased(evt)
        return True
           
