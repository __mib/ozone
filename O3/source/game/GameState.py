import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI
from IngameGUIManager import IngameGUIManager

class GameState(object):   
    def __init__(self, manager):
        self.mStateManager = manager
 
    def start(self):
        print "a state starts"
        self.resume()
    
    def stop(self):
        self.suspend()  
    
    def resume(self):
        pass
    
    def suspend(self):
        pass

class DotScene(GameState):
    def __init__(self, manager, 
                       root, 
                       window, 
                       ceguiSystem, 
                       cameraMan, 
                       sceneElementFactory, 
                       sceneIOListener, 
                       timeEventManager, 
                       dotSceneParser,
                       physicsWrapper,
                       frameListener,
                       level,
                       characterControl,
                       mSoundManager):
        self.mStateManager = manager
        self.root = root
        self.window = window
        self.ceguiSystem = ceguiSystem
        self.cameraMan = cameraMan
        self.parser = dotSceneParser
        self.frameListener = frameListener
        self.sceneIOListener = sceneIOListener
        self.level = level
        self.characterControl = characterControl
        self.timeEventManager = timeEventManager
        self.mSoundManager = mSoundManager
        self.viewport = None
        self.compositors = set()
    
    def init(self):
        self.viewportBackgroundColor =  self.parser.getBackgroundColor()
        self.sheet = CEGUI.WindowManager.getSingleton().loadWindowLayout("o3SceneInterace.layout")
        windowManager = CEGUI.WindowManager.getSingleton()
        self.gui = IngameGUIManager(self.sheet, windowManager)
        self.gui.setTimeEventManager(self.level.getTimeEventManager())
        self.level.gui = self.gui
        self.level.mGameState = self
        self.level.registerCallbacks(self.characterControl)
        self.level._onLevelLoaded()

    def finish(self, **kwargs):
        self.frameListener.scheduleWorldsEnd()
    
    def _updateCompositors(self):
        if not self.viewport:
            return
        for cs in self.compositors:
             Ogre.CompositorManager.getSingleton().addCompositor(self.viewport, cs)
             Ogre.CompositorManager.getSingleton().setCompositorEnabled(self.viewport, cs, True)     
       
    
    def addCompositor(self, name):
        self.compositors.add(name)
        self._updateCompositors()
    
    def removeCompositor(self, name):
        self.compositors.remove(name)
        Ogre.CompositorManager.getSingleton().setCompositorEnabled(self.viewport, name, False) 
        self._updateCompositors()
    
    def resume(self):
        print "dot resuming"
        self.window.removeAllViewports()
        self.viewport = self.window.addViewport(self.cameraMan.getCamera())
        print self.viewportBackgroundColor
        if self.viewportBackgroundColor:
            self.viewport.setBackgroundColour(self.viewportBackgroundColor)
       # self.camera.setAspectRatio(self.viewport.getActualWidth() / self.viewport.getActualHeight())
        self.root.addFrameListener(self.frameListener)
        self.sceneIOListener.focus()
        self.ceguiSystem.setGUISheet(self.sheet)
        self.level.viewport = self.viewport
        self._updateCompositors()
    
    def stop(self):
        print "Stopping the GameState..."
        
        self.suspend()
        
        """
            self.viewportBackgroundColor =  self.parser.getBackgroundColor()
        self.sheet = CEGUI.WindowManager.getSingleton().loadWindowLayout("o3SceneInterace.layout")
        windowManager = CEGUI.WindowManager.getSingleton()
        self.gui = IngameGUIManager(self.sheet, windowManager)
        self.gui.setTimeEventManager(self.level.getTimeEventManager())
        self.level.gui = self.gui
        self.level.mGameState = self
        self.level.registerCallbacks(self.characterControl)
        self.level._onLevelLoaded()
        """
        CEGUI.WindowManager.getSingleton().destroyWindow(self.sheet)
        
        # TODO: zniszczyc reszte
        world = self.level.getWorld()
        world.destroyAll()
        self.level.destroyAll()
        sceneManager = self.level.getSceneManager()
        self.mSoundManager.destroyAll()
        del self.mSoundManager
        
        """
                sceneManager = self.root.createSceneManager(Ogre.ST_GENERIC, "SampleSceneManager")
       .clearScene()
        ioListener = SceneIOListener.SceneIOListener(self.mouse, self.keyboard, self.log)
        timeEventMgr = TimeEventManager.TimeEventManager(self.log)
        cameraMan = CameraMan.CameraMan("dotscenecameraman", sceneManager, camera)
        ioListener.registerSecondary(cmListener)
        soundManager = SoundManagerListener(sceneManager, camera)
        timeEventMgr.registerOnNewFrame(soundManager)
    
        physics = PhysicsWrapper.PhysicsWorld(sceneManager)

        frameListener = SceneFrameListener(self.window, camera, self.keyboard, self.mouse, sceneManager, physics, timeEventMgr)
        

        mAIManager = AIControl.AIManager(timeEventMgr)
        mTerrainClickLocator = TerrainClickLocator.TerrainClickLocator(camera, sceneManager, self.window)
        level = SampleLevel(sceneManager, cameraMan, ioListener)
        levelFac = Level.LevelElementFactory(level, sceneManager, mAIManager, mTerrainClickLocator)
        dsSceneElementFactory = DotSceneParser.SceneElementFactory(sceneManager)
        dsParser = DotSceneParser.DotSceneParser("scex.scene", "SampleScene", sceneManager, physics, dsSceneElementFactory, levelFac, self.log)
        charControl = WSADCharacterControl(cameraMan)
        ioListener.registerSecondary(charControl)
        selLis = levelFac.buildControlSelectionListener(charControl)
        level.charControl = charControl
        
        colDisp = Level.CollisionDispatcher(level)
        physics.setCollisionCallback(colDisp)
        
        level.setWorld(physics)
        level.setTimeEventManager(timeEventMgr)
        level.mStateManager = self.manager
        if level.getSettings().get('selectorAvailable', False):
            selector = SceneObjectSelector.SceneObjectSelector(cameraMan, sceneManager, self.log)
            selector.setListener(selLis)
            ioListener.registerSecondary(selector) 
       
        ioListener.registerSecondary(mTerrainClickLocator)
        mSampleTerrainClickLocatorListener = TerrainClickLocator.SampleTerrainClickLocatorListener(sceneManager, soundManager)
        mTerrainClickLocator.registerListener(mSampleTerrainClickLocatorListener)

        plane = Ogre.Plane((0, -1, 0), -5000)
        sceneManager.setSkyPlane(True, plane, level.getSettings()['skyPlane'], 100, 1)
        
        dsParser.build()
        
        """
        import gc
        
        self.timeEventManager.destroyAll()
        del self.timeEventManager
        
        self.parser.destroyAll()
        
        self.sceneIOListener.destroyAll()
        del self.sceneIOListener
        
        sceneManager.clearScene()
        
        del self.cameraMan
        del self.level
        
        
        
        
        self.GAME_STATE = "IGNORE"
        
        print "GameState cleanup ALMOST done, debugging info follows:"
 
        print "Referrers to dotSceneParser"
        for x in gc.get_referrers(self.parser):
            print x    
 
        del self.parser
 
        print "Referrers to physicsWrapper"
        for x in gc.get_referrers(world):
            print x    
        
        del world
        
        print "Referrers to sceneManager"
        for x in gc.get_referrers(sceneManager):
            print x
        
        self.root.destroySceneManager(sceneManager)
        
        print "Referrers to this state"
        for x in gc.get_referrers(self):
            print x

        
        #print "Sleeping..."
        #import time
        #time.sleep(1)
        #print "deleting world"
        
        #time.sleep(1)
        
        #gc.set_debug(gc.DEBUG_SAVEALL)
        print "Cleanup ended..."
        
        """
                self.mStateManager = manager
        self.root = root
        self.window = window
        self.ceguiSystem = ceguiSystem
        self.cameraMan = cameraMan
        self.parser = dotSceneParser
        self.frameListener = frameListener
        self.sceneIOListener = sceneIOListener
        self.level = level
        self.characterControl = characterControl
    
    def init(self):
        self.viewportBackgroundColor =  self.parser.getBackgroundColor()
        self.sheet = CEGUI.WindowManager.getSingleton().loadWindowLayout("o3SceneInterace.layout")
        windowManager = CEGUI.WindowManager.getSingleton()
        self.gui = IngameGUIManager(self.sheet, windowManager)
        self.gui.setTimeEventManager(self.level.getTimeEventManager())
        self.level.gui = self.gui
        self.level.registerCallbacks(self.characterControl)
        self.level._onLevelLoaded()
        """
    
    
    def suspend(self):
        self.root.removeFrameListener(self.frameListener)
        self.window.removeAllViewports()
        self.sceneIOListener.defocus()
        self.viewport = None
        self.ceguiSystem.setGUISheet(None)
