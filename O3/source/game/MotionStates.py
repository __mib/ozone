import ogre.physics.bullet as bullet
import ogre.renderer.OGRE as Ogre

"""
    Bullet <-> Ogre
"""

class OgreMotionState(bullet.btMotionState): 
    def __init__(self, initialPosition, node): 
        bullet.btMotionState.__init__(self)
        self.Pos = initialPosition
        self.node = node
        
    def getWorldTransform(self, WorldTrans): 
        WorldTrans.setOrigin(self.Pos.getOrigin())
        WorldTrans.setBasis(self.Pos.getBasis())
          
    def setWorldTransform(self, worldTrans):
        rot = worldTrans.getRotation();
        self.node.setOrientation(rot.w(), rot.x(), rot.y(), rot.z());
        pos = worldTrans.getOrigin();
        self.node.setPosition(pos.x(), pos.y(), pos.z());

class NoRotOgreMotionState(bullet.btMotionState): 
    def __init__(self, initialPosition, node): 
        bullet.btMotionState.__init__(self)
        self.Pos = initialPosition
        self.node = node
        
    def getWorldTransform(self, WorldTrans): 
        WorldTrans.setOrigin(self.Pos.getOrigin())
        WorldTrans.setBasis(self.Pos.getBasis())
          
    def setWorldTransform(self, worldTrans):
        pos = worldTrans.getOrigin();
        self.node.setPosition(pos.x(), pos.y(), pos.z());
