from CooldownDecorator import CooldownWard
from RunInSomeTime import RunMethodInSomeTime

"""
    Klasa rozszerzajaca Actor
"""

class PCSkills:
    
    def setUsableSkills(self, name_list):
        self._skills_usable = name_list
        if not hasattr(self, '_skills_inited'):
            self._skills_inited = set()
        for x in name_list:
            if not x in self._skills_inited:
                PCSkills.skills_init[x](self)
                self._skills_inited.add(x)
            
    
    def getUsableSkills(self):
        if not hasattr(self, '_skills_usable'):
            return list()
        return self._skills_usable
    
    def getAllSkills(self):
        return PCSkills.skills.keys()
    
    """
        Przed wywolaniem tej funkcji nalezy wywolac setUsableSkills
    """
    def useSkill(self, skill_name, **kwargs):
        if skill_name not in self._skills_usable:
            if skill_name not in PCSkills.skills:
                print "Skill", skill_name, "does not exist!"
            else:
                print "Skill", skill_name, "is not registered for use with this Actor"
        PCSkills.skills[skill_name](self, **kwargs)
        
    @CooldownWard(0.1)
    def _skill_healing(self, health_per_tick = 10.0, usage_cost=6.0):
        if self.getEnergy() < usage_cost:
            return
        self.modEnergy(-usage_cost)
        self.onDamageAbsorbed( -health_per_tick )
        self._skill_healing_ps_extinguisher.reschedule(0.2)
        self._skill_healing_ps.setEmitting(True)
    
    def _skill_healing_disable_ps(self):
        self._skill_healing_ps.setEmitting(False)
    
    def _skill_healing_init(self):
        self._skill_healing_ps = self.getLevel().getSceneManager().createParticleSystem(self.getNode().getName() + "_healingPS", "Spiral")
        
        scale_node = self.getLevel().getSceneManager().createSceneNode()
        scale_node.setScale(0.1, 0.1, 0.1)
        scale_node.attachObject(self._skill_healing_ps)
        
        self.getNode().addChild(scale_node)
        
        self._skill_healing_ps.setEmitting(False)
        self._skill_healing_ps_extinguisher = RunMethodInSomeTime(0.2, self.getTimeEventManager(), PCSkills._skill_healing_disable_ps, [self], {}, active_at_startup=False)
        
    
    
        
    GENERIC_HEALING = 'healing'
    
    skills = {
              'healing': _skill_healing
    }
    
    skills_init = {
                   'healing': _skill_healing_init
    }
    