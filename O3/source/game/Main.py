
import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI

from StateManager import StateManager
from StateFactory import StateFactory

RESOURCES_FILE = "resources.cfg"
WINDOW_TITLE = "o3 DEV"

class Application(object):
    def __init__(self):
        pass
    
    def run(self, forceConfigDialog=False):
        self.root = Ogre.Root()
        self.root.loadPlugin("OgreOggSound")
        self.log = Ogre.LogManager.getSingleton().getDefaultLog()
      
        self.log.logMessage("Parsing RESOURCES_FILE", Ogre.LML_NORMAL)  
        cf = Ogre.ConfigFile()
        cf.load(RESOURCES_FILE)
        seci = cf.getSectionIterator()
      
        while seci.hasMoreElements():
            secName = seci.peekNextKey()
            settings = seci.getNext()
            for item in settings:
                typeName = item.key
                archName = item.value
                Ogre.ResourceGroupManager.getSingleton().addResourceLocation(archName, typeName, secName)
    
    
        self.log.logMessage("Restoring configuration", Ogre.LML_NORMAL)
        if ( forceConfigDialog or not self.root.restoreConfig() ) and (not self.root.showConfigDialog()):
            self.log.logMessage("Launch aborted by user", Ogre.LML_NORMAL)
            return False
        
        self.root.initialise(True, WINDOW_TITLE)
        self.window = self.root.getAutoCreatedWindow()
        
        self.initBaseResources()
        
        self.initOIS()
        self.initCEGUI()
        
        self.stateManager = StateManager()
        self.stateFactory = StateFactory(self.root, self.window, self.stateManager, self.system, self.inputManager, self.mouse, self.keyboard, self.log)
        self.stateManager.push(self.stateFactory.newMainMenuInstance())
        #self.stateManager.push(self.stateFactory.newDotSceneState())
        
        self.root.startRendering()
    
    def initOIS(self):
        windowHandle = 0
        renderWindow = self.root.getAutoCreatedWindow()
        windowHandle = renderWindow.getCustomAttributeInt("WINDOW")
        paramList = [("WINDOW", str(windowHandle))]
        self.inputManager = OIS.createPythonInputSystem(paramList)
        self.keyboard = self.inputManager.createInputObjectKeyboard(OIS.OISKeyboard, True)
        self.mouse = self.inputManager.createInputObjectMouse(OIS.OISMouse, True)

    
    def initCEGUI(self):
        self.log.logMessage("CEGUI init", Ogre.LML_NORMAL)
        self.renderer = CEGUI.OgreRenderer.bootstrapSystem()
        self.system = CEGUI.System.getSingleton()
        CEGUI.SchemeManager.getSingleton().create("TaharezLook.scheme")
        CEGUI.SchemeManager.getSingleton().create("VanillaSkin.scheme")
        self.system.setDefaultMouseCursor("TaharezLook", "MouseArrow")
        self.system.setDefaultFont("DejaVuSans-10")
        
    def initBaseResources(self):
        Ogre.TextureManager.getSingleton().setDefaultNumMipmaps(5)
        #TODO: to nie powinno tak wygladac
        Ogre.ResourceGroupManager.getSingleton().initialiseAllResourceGroups()
        
if __name__ == '__main__':
    print "o3 Startup"
    app = Application()
    app.run()
    
    #import cProfile
    #cProfile.run('app.run()', 'log.txt')
    #import pstats
    #p = pstats.Stats('log.txt')
    #p.strip_dirs().sort_stats('cumulative').print_stats()
    
