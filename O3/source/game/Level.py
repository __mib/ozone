import ogre.renderer.OGRE as Ogre
import SceneIOListener
import ogre.physics.bullet as bullet
import TimeEventManager
import SceneObjectSelector
import ogre.io.OIS as OIS
import PhysicsWrapper
from datetime import datetime, timedelta
import time
import sys

class Actor(object):
    def __init__(self, xml=None, parent=None, sceneFactory=None, levelFactory=None, parser=None):
        if not xml:
            return
        node = xml
        parentEntity = parent
        obj = parser.elementFactory.makeNode(node.getAttribute("name"), parentEntity)
        levelFactory.registerSceneActor(obj, self)
        for x in node.childNodes:
            parser.nodeChildDispatch.get(x.nodeName, parser.handlerNotFound)(x, obj)    
        self.node = obj
        self.levelFactory = levelFactory
        self.parser = parser
        self.sceneFactory = sceneFactory
    
    def getLevel(self):
        return self.getLevelFactory().getLevel()
    
    def getTimeEventManager(self):
        return self.getLevelFactory().getLevel().getTimeEventManager()

    def getLevelFactory(self):
        return self.levelFactory
    
    def setBody(self, body):
        self.body = body
    
    def setNode(self, node):
        self.node = node
    
    def getNode(self):
        return self.node
    
    def getBody(self):
        return self.body
    
    def collisionEvent(self, actor2):
        pass
        #print "Collision with:", actor2

    def onLevelLoaded(self):
        pass

class Movable(Actor):
    def setDriver(self, driver):
        self.driver = driver
        
    def getDriver(self):
        return self.driver

class Ghost(Actor):
    pass

class SpawnableActor(Actor):
    def spawn(self, parentNode, mLevelElementFactory):
        pass

class Mortal(Actor):
    def __init__(self, **kwargs):
        super(Mortal, self).__init__(**kwargs)
        self.currentHP = 1
        self.maxHP = 1
    
    def setHP(self, value):
        self.currentHP = value
    
    def setMaxHP(self, value):
        self.maxHP = value
    
    def getMaxHP(self, value):
        self.maxHP = value
    
    def getHP(self):
        return self.currentHP
    
    def onDeath(self):
        print "UMIERAM!", self
        self.getLevelFactory().removeActor(self)
    
    def onDamageAbsorbed(self, value):
        self.currentHP -= value
        if self.currentHP > self.maxHP:
            self.currentHP = self.maxHP
            
        if self.currentHP <= 0:
            self.currentHP = 0
            self.onDeath()

class ActivatableActor(Actor):
    def activate(self):
        pass
        
class Character(Actor):
    pass
    #def __init__(self):
    #    pass
    

class Weapon(ActivatableActor):
    counter = 0
    name = "Unknown weapon"
    cooldown = 0 # microseconds
    ammo = 0
    last_used = datetime.now() # time in microseconds
    force = 0 # N
    mass = 0 # kg
    max_ammunition = 0
    shortcut = 0 # num in weapons list
    def init(self, node):
        self.node = node;
        
    def activate(self, target):
        pass
    def is_usable(self):
        if self.ammo > 0 and datetime.now() - timedelta(microseconds=self.cooldown) >= self.last_used :
            return True
        return False
    def use(self):
        self.last_used = datetime.now()
        self.ammo = self.ammo - 1
    def collect_weapon(self, w):
        self.ammo = min(self.max_ammunition, self.ammo + w.ammo)
        self.last_used = min(self.last_used, w.last_used)
    def collect_ammunition(self, a):
        self.ammo = min(self.max_ammunition, self.ammo + a.ammo)
 
class Bullet(Actor):
    counter = 0        
    name = "Unknown bullet"
    damage = 0
    mass = 0.1 # kg
    def collisionEvent(self, actor2):
        print actor2
        if isinstance(actor2, Mortal):
            actor2.onDamageAbsorbed(self.damage)
    def getEntity(self):
        return self.entity
    


class Trigger(object):
    def fire(self):
        pass

class ControlSelectionListener(SceneObjectSelector.SceneObjectSelectedEvent):
    def __init__(self, control, level):
        self.control = control
        self.level = level
        
    def objectSelectedEvent(self, node):
        self.level.printInfo(node)
        self.control.setCharacter( self.level.actorForNode(node) )
        
"""
class GenericCharacterControl(SceneIOListener.SecondaryIOListener):
    def __init__(self, cameraMan, character=None):
        self.setCharacter(character)
        self.cameraMan = cameraMan
        self.triggers = dict()
    
    def setCharacter(self, character):
        print "GenericControl: taking control of", character
        self.character = character
    
    def registerTrigger(self, key, trigger):
        self.triggers[key] = trigger
    
    def unregisterTrigger(self, key):
        pass
    
    def handleMovement(self, evt):    
        if evt.key <> OIS.KC_1:
            return False
        if self.character:
            print "apply!"
            campos = self.cameraMan.cameraNode.getPosition()
            objpos = self.character.node.getPosition()
            fvec = objpos - campos
            fvec.y = 0
            fvec.normalise()
            fvec *= 10
            print fvec.x, fvec.y, fvec.z
            self.character.body.activate()
            self.character.body.applyCentralImpulse(bullet.btVector3(fvec.x, fvec.y, fvec.z))
        

    def keyPressed(self, evt):
        if self.handleMovement(evt):
            return
        trigger = self.triggers.get(evt.key, None)
        if trigger:
            trigger.fire()

    def keyReleased(self, evt):
        pass

"""

class WeaponShootTrigger(Trigger):
    def __init__(self, weaponSelection):
        self.weaponSelection = weaponSelection
    
    def fire(self):
        pass

class WeaponSelection():
    def __init__(self):
        self.weaponList = list()
        self.selectedIndex = 0
    
    def getCurrent(self):
        return self.weaponList[self.selectedIndex]
    
    def addWeapon(self, weapon):
        self.weaponList.append(weapon)
    
    def nextWeapon(self):
        if len(self.weaponList) >= 1 + self.selectedIndex:
            self.selectedIndex = 0
        else:
            self.selectedIndex += 1
      
class WeaponSelectionNext(Trigger):
    def __init__(self, mWeaponSelection):
        self.mWeaponSelection = mWeaponSelection
        
    def fire(self):
        self.mWeaponSelection.nextWeapon()

class LevelElementFactory(object):    
    def __init__(self,level,sceneManager, mAIManager, mTerrainClickLocator):   
        self.level = level
        self.node_actor = dict()
        self.body_actor = dict()
        self._body_cache = dict()
        self.level._setFactory(self)
        self.sceneManager = sceneManager
        self.mAIManager = mAIManager
        self.body_slider = dict()
        self.mTerrainClickLocator = mTerrainClickLocator
    
    def removeActor(self, actor):
        onode = actor.getNode()
        node = onode.getName()
        body = actor.getBody()
        print "remove ", onode, node, body
        if node in self.node_actor:
            print "removing from node_actor"
            del self.node_actor[node]
        if body in self.body_actor:
            print "removing from body_actor"
            del self.body_actor[body]
        if node in self._body_cache:
            print "removing from world"
            self.level.getWorld().removeBody(body)
            del self._body_cache[node]
        onode.removeAndDestroyAllChildren()
        self.sceneManager.destroySceneNode(onode)
        # ToDo: NALEZY TEZ POUSUWAC ZAPARENTOWANE ENTITY!
        
    def getAIManager(self):
        return self.mAIManager
    
    def getLevel(self):
        return self.level
    
    def nodeForName(self, name):
        return self.sceneManager.getSceneNode(name)
    
    def buildControlSelectionListener(self, control):
        return ControlSelectionListener(control, self.level)
    
    def registerActorProperty(self, node, property):
        pass
    
    
    
    def registerSceneActor(self, node, actor):
        print "Register Actor ",node, actor
        node = node.getName()
        self.node_actor[node] = actor
        if node in self._body_cache:
            body = self._body_cache[node]
            actor.setBody(body)
            self.body_actor[body] = actor
    
    def bodyForNode(self, node_name):
        return self._body_cache.get(node_name, None)
    
    def registerHinge(self, body, hinge):
        pass
    
    def getSlider(self, body):
        return self.body_slider[body]
    
    def registerSlider(self, body, slider):
        self.body_slider[body] = slider
    
    def destroyAll(self):
        del self.level
        del self.node_actor
        del self.body_actor
        del self._body_cache
        del self.sceneManager
        del self.mAIManager
        del self.mTerrainClickLocator
    
    def registerActorsBody(self, node, body):
        print "Register Body", node.getName(), body
        node = node.getName()
        if node not in self._body_cache:
            self._body_cache[node] = body
        if node not in self.node_actor:
            self._body_cache[node] = body
        else:
            self.node_actor[node].setBody(body)
            self.body_actor[body] = self.node_actor[node]
    
    def delegateCreation(self, xmlnode, parentEntity, sceneElementFactory, dotParser):
        print "delegateCreation", xmlnode, parentEntity
        #import actors.Crates
        #import actors.Mobs
        
        classo = None
        ws = dotParser.getFromUserData(xmlnode, 'actorClass')
        module_name, separator, class_name = ws.rpartition('.')
        try:
            __import__(module_name)
            module = sys.modules[module_name]
            classo = module.__dict__[class_name]
        except Exception as e:
            print "Problem obtaining class", class_name, "from module", module_name
            print e, e.args, e.message
            exit(1)
    
        actor = classo(xml=xmlnode, parent=parentEntity, sceneFactory=sceneElementFactory, levelFactory=self, parser=dotParser)
        print actor
        
        #(xml=xmlnode, parent=parentEntity, sceneFactory=sceneElementFactory, levelFactory=self, parser=dotParser) #globals()[str(model)]()
        #actor.setNode(node)
        #node = node.getName()

        #actors.Crates.WoodenCrate
        
    
class CollisionDispatcher(PhysicsWrapper.CollisionCallback):
    def __init__(self, level):
        self.level = level
        
    def bodiesCollided(self, body1, body2):
        #print "collision, bodies ", body1, body2
        actor1 = self.level.actorForBody(body1)
        actor2 = self.level.actorForBody(body2)
        #print "corresponding ", actor1, actor2
        if actor1 and actor2:
            actor1.collisionEvent(actor2)
            actor2.collisionEvent(actor1)
        

class Level(object):
    def __init__(self, sceneManager, cameraMan, listener):
        self.sceneManager = sceneManager
        self.cameraMan = cameraMan
        self.listener = listener
        self.phWorld = None
        self.factory = None
        self.gameEnded = False
    
    def setMainIOListener(self, listener):
        self.main_io_listener = listener
        
    def getMainIOListener(self):
        return self.main_io_listener
    
    def getGameState(self):
        return self.mGameState
    
    def getSceneManager(self):
        return self.sceneManager
    
    def really_finish(self):
        self.getGameState().finish()
    
    def finish(self, **kwargs):
        if self.gameEnded:
            return
        self.gameEnded = True
        self.getGUI().displayText("GAME OVER\n%s\n" % kwargs.get('message', ''))
        self.getTimeEventManager().registerCallbackIn( TimeEventManager.RefTEListener(Level.really_finish, self), 2.0 )
        
    
    def getGUI(self):
        return self.gui
        
    def destroyAll(self):
        self.factory.destroyAll()
        del self.cameraMan
        del self.listener
        del self.phWorld
        del self.factory
    
    def setWorld(self, phWorld):
        self.phWorld = phWorld
    def getWorld(self):
        return self.phWorld
    def actorForBody(self, body):
        if self.factory:
            return self.factory.body_actor.get(body, None)
    
    def printInfo(self, node):
        print "Info for node", node, node.getName()
        actornode, actor = self.getActorNode(node)
        print "Actor data:"
        anname = None
        if actornode:
            anname = actornode.getName()
        print "node:", actornode, anname
        print "actor: ", actor
        print "Original body data:"
        print self.factory.bodyForNode(node)
        print "Actor body data:"
        print self.factory.bodyForNode(actornode)
    
    def _onLevelLoaded(self):
        self.onLevelLoaded()
        for node, actor in self.factory.node_actor.items():
            actor.onLevelLoaded()
    
    def onLevelLoaded(self):
        pass
        
    def registerCallbacks(self, mGenericCharacterControl):
        pass
    
    def actorForName(self, name):
        return self.factory.node_actor.get(name, None)
    
    """ Traverses the scene tree upwards looking for a node registered as and actor, returns (node, actor) """
    def getActorNode(self, node):
        if isinstance(node, Ogre.Entity):
            node = node.getParentNode()
        if self.factory:
            result = None
            while result == None and node <> None:
                #print "Looking for node ", node, node.getName()
                result = self.factory.node_actor.get(node.getName(), None)
                node = node.getParent()
        return (node, result)
 
    def actorForNode(self, node):
        return self.getActorNode(node)[1]
    
    def setTimeEventManager(self, o):
        self.timeEventManager = o
    
    def getTimeEventManager(self):
        return self.timeEventManager
        
    _settings = {
                'selectorAvailable': True,
    }
    
    def setSoundManager(self, mSoundManager):
        self.mSoundManager = mSoundManager
    def getSoundManager(self):
        return self.mSoundManager
    def getSettings(self):
        return self._settings
    def getListener(self):
        return self.listener   
    def _setFactory(self, factory):
        self.factory = factory
