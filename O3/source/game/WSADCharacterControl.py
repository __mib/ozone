import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS
import SceneIOListener


class WSADCharacterControl(SceneIOListener.SecondaryIOListener):
    def __init__(self, ioListener, cameraMan, character=None):
        self.setCharacter(character)
        self.ioListener = ioListener
        self.cameraMan = cameraMan
        self.triggers = dict()
        self.epsilon = 10e-5
        self.current_force = Ogre.Vector3(0,0,0)
    
    def destroyAll(self):
        del self.character
        del self.cameraMan
        del self.triggers
    
    def setCharacter(self, character):
        print "WSADControl: taking control of", character
        self.character = character
        
    def getForce(self):
        camera_position = self.cameraMan.getCamera().getRealPosition()
        object_position = self.character.getNode().getPosition()
        #print "camera", camera_position, "object", object_position
        displacement = object_position - camera_position
        displacement_onplane = Ogre.Vector3(displacement.x, displacement.y, 0.0)
        #print "Wektor kamera->obiekt", displacement, "po rzutowaniu", displacement_onplane
        if displacement_onplane.squaredLength() < self.epsilon:
            print "WSADCC: camera horizon"
            return Ogre.Vector3(0,0,0)
        
        forward = displacement_onplane.normalisedCopy()
        backward = -1.0 * forward
        left = -1.0 * Ogre.Vector3(forward.y, -1.0 * forward.x, 0.0)
        right = -1.0 * left
        
        impulse_direction = Ogre.Vector3(0,0,0)
        
        if self.ioListener.isKeyPressed(OIS.KC_W):
            impulse_direction += forward
        if self.ioListener.isKeyPressed(OIS.KC_S):
            impulse_direction += backward
        if self.ioListener.isKeyPressed(OIS.KC_A):
            impulse_direction += left
        if self.ioListener.isKeyPressed(OIS.KC_D):
            impulse_direction += right
            
        return impulse_direction.normalisedCopy()*2
    
    interestingKeys = (OIS.KC_W, OIS.KC_S, OIS.KC_A, OIS.KC_D)
    
    def applyForce(self):
        impulse_direction = self.getForce()
        self.character.getDriver().stopMovement(self.current_force)
        self.current_force = impulse_direction
        self.character.getDriver().startMovement(impulse_direction)

    def keyPressed(self, evt):
        if not self.character: return
        
        # TODO: debug, wykasowac kiedys:
        if evt.key == OIS.KC_T:
            self.character.getLevelFactory().getLevel().finish(reason="debug exit")
        
        if evt.key == OIS.KC_SPACE:
            self.character.getDriver().jump()
        
        self.applyForce()
        #self.character.getDriver().induceMovement(impulse_direction);
        
    def keyReleased(self, evt):
        if not self.character: return
        self.applyForce()

