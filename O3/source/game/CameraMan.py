import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS
import SceneIOListener
import TimeEventManager
import threading

class StandardCamera():
    def __init__(self, cameraMan, ioListener):
        self.cameraMan = cameraMan
        self.ioListener = ioListener
    
    def start(self):
        self.cameraMan.setCameraType("indirect")
    
    def stop(self):
        pass
        
    def onTimeEvent(self, time, foo):
        dx = dy = dz = 0.0
        if self.ioListener.isKeyPressed(OIS.KC_W):
            dz = -time * 20.0
        if self.ioListener.isKeyPressed(OIS.KC_S):
            dz = time * 20.0
        if self.ioListener.isKeyPressed(OIS.KC_A):
            dx = -time * 20.0
        if self.ioListener.isKeyPressed(OIS.KC_D):
            dx = time * 20.0
            
            
        #print self.cameraMan.cameraNode.getPosition().x
        """self.cameraMan.instantUpdate( 
                                     (self.cameraMan.cameraNode.getPosition().x+dx,
                                      self.cameraMan.cameraNode.getPosition().y+dy,
                                      self.cameraMan.cameraNode.getPosition().z+dz),
                                            (self.cameraMan.cameraNode.getPosition().x+10,
                                     0, 0) )
        """
        #self.cameraMan.targetNode.translate(self.cameraMan.targetNode.getOrientation() * Ogre.Vector3(dx, dy, dz))
        #self.cameraMan.cameraNode.setPosition(self.cameraMan.targetNode.getPosition() + self.cameraMan.targetNode.getOrientation()*Ogre.Vector3(0,40,40))
        #self.cameraMan.cameraNode.setOrientation(self.cameraMan.targetNode.getOrientation())
        self.cameraMan.cameraNode.setPosition(self.cameraMan.targetNode.getPosition() + Ogre.Vector3(0,-40,40))
        
        self.cameraMan.cameraNode.setAutoTracking(True, self.cameraMan.targetNode)
        #self.cameraMan.cameraNode.roll(3.14)
        #self.cameraMan.cameraNode.pitch(1)
    
    def mouseMoved(self, evt):
        #self.cameraMan.cameraNode.setAutoTracking(False)
        #self.cameraMan.targetNode.yaw( -evt.get_state().X.rel / 300.0 )
        #self.cameraMan.targetNode.pitch( -evt.get_state().Y.rel / 300.0 )
        return True

class MouseCamera():
    def __init__(self, cameraMan, ioListener):
        self.cameraMan = cameraMan
        self.ioListener = ioListener
        self.delta = Ogre.Vector3(0,-30,30)
    
    def start(self):
        self.cameraMan.setCameraType("indirect")
    
    def stop(self):
        pass
        
    def onTimeEvent(self, time, foo):
        self.delta = self.delta.normalisedCopy()*1.414*30
        self.cameraMan.cameraNode.setPosition(self.cameraMan.targetNode.getPosition() + self.delta)
        
        self.cameraMan.camera.setAutoTracking(True, self.cameraMan.targetNode)
        
    def mouseMoved(self, evt):
        self.cameraMan.camera.setFixedYawAxis(True, Ogre.Vector3(0,0,1))
        yaxis = Ogre.Vector3(0,0,1)
        
        roty = Ogre.Quaternion(-evt.get_state().X.rel / 300.0, yaxis)
        rotz = Ogre.Quaternion(-evt.get_state().Y.rel / 300.0, -self.delta.crossProduct(yaxis).normalisedCopy())
        
        self.delta = rotz*roty*self.delta
        return True

class AimCamera():
    def __init__(self, cameraMan, ioListener):
        self.cameraMan = cameraMan
        self.ioListener = ioListener
        self.delta = Ogre.Vector3(0,-1,0)
        
    def start(self):
        self.cameraMan.setCameraType("direct")
        #self.cameraMan.targetNode.setVisible(False)
        self.cameraMan.sceneManager.getSceneNode("SampleHero").setVisible(False)
        self.cameraMan.cameraNode.setPosition(self.cameraMan.targetNode.getPosition() + self.delta)
    
    def stop(self):
        #self.cameraMan.targetNode.setVisible(True)
        self.cameraMan.sceneManager.getSceneNode("SampleHero").setVisible(True)
        pass
        
    def onTimeEvent(self, time, foo):
        self.delta = self.delta.normalisedCopy()
        self.cameraMan.cameraNode.setPosition(self.cameraMan.targetNode.getPosition() + self.delta)
        
        self.cameraMan.camera.setAutoTracking(True, self.cameraMan.targetNode)
        #print "camera dir", self.cameraMan.camera.getDirection()
        #print "camera derdir", self.cameraMan.camera.getDerivedDirection()
        
    def mouseMoved(self, evt):
        self.cameraMan.camera.setFixedYawAxis(True, Ogre.Vector3(0,0,1))
        yaxis = Ogre.Vector3(0,0,1)
        
        roty = Ogre.Quaternion(-evt.get_state().X.rel / 300.0, yaxis)
        rotz = Ogre.Quaternion(-evt.get_state().Y.rel / 300.0, -self.delta.crossProduct(yaxis).normalisedCopy())
        self.delta = rotz*roty*self.delta
        self.delta = self.delta.normalisedCopy()
        self.cameraMan.cameraNode.setPosition(self.cameraMan.targetNode.getPosition() + self.delta)
        return True

class SampleCameraManInputListener(SceneIOListener.SecondaryIOListener, TimeEventManager.TimeEventListener):
    def __init__(self, cameraMan, ioListener):
        self.cameraMan = cameraMan
        self.ioListener = ioListener
        self.cameraList = [StandardCamera(cameraMan, ioListener), MouseCamera(cameraMan, ioListener), AimCamera(cameraMan, ioListener)]
        self.camera = self.cameraList[0]
        self.index = 0
        self.lock = threading.RLock()
    
    def destroyAll(self):
        if hasattr(self, 'cameraMan'):
            del self.cameraMan
        if hasattr(self, 'ioListener'):
            del self.ioListener
    
    def onTimeEvent(self, time, foo):
        self.lock.acquire()
        self.camera.onTimeEvent(time, foo)
        self.lock.release()
    
    def mouseMoved(self, evt):
        self.lock.acquire()
        self.camera.mouseMoved(evt)
        self.lock.release()
    
    def swap_camera(self):
        self.lock.acquire()
        self.index = (self.index+1)%len(self.cameraList)
        self.camera.stop()
        self.camera = self.cameraList[self.index]
        self.camera.start()
        self.lock.release()
          
    def keyPressed(self, evt):
        if evt.key == OIS.KC_ESCAPE:
            exit()
        if evt.key == OIS.KC_TAB:
            self.swap_camera()
        return True
        
class CameraMan(object):

    def __init__(self, name, sceneManager, camera):
        self.name = name
        self.sceneManager = sceneManager
        self.cameraNode = self.sceneManager.getRootSceneNode() \
                                            .createChildSceneNode(self.name)
        self.targetNode = self.cameraNode
        self.targetNode.setVisible(False)
        #self.targetNode = self.sceneManager.getRootSceneNode() \
        #                                    .createChildSceneNode(self.name
        #                  
        #                                                            + "_target")
        #self.cameraNode.setAutoTracking(True, self.targetNode)
        #self.cameraNode
        #self.cameraNode.setFixedYawAxis(True)
        self.cameraType = "indirect"
 
        self.camera = camera
        self.camera.setPosition(0,0,0)
 
        self.cameraNode.attachObject(self.camera)
        self.tightness = 0.1
    
    def getCamera(self):
        return self.camera
    
    def setCameraType(self, type):
        self.cameraType = type
    
    def getCameraType(self):
        return self.cameraType
    
    def setTarget(self, targetNode):
        self.targetNode = targetNode
        #self.cameraNode.setAutoTracking(True, self.targetNode)
        
    def instantUpdate(self, cameraPosition, targetPosition):
        self.cameraNode.setPosition(cameraPosition)
        self.targetNode.setPosition(targetPosition)
        
    def update(self, elapsedTime, cameraPosition, targetPosition):
        
        """displacement = (Ogre.Vector3(cameraPosition[0], cameraPosition[1], cameraPosition[2]) - self.cameraNode \
                                                        .getPosition()) \
                                                        * self.tightness
        self.cameraNode.translate(displacement)
 
        displacement = (Ogre.Vector3(targetPosition[0], targetPosition[1], targetPosition[2]) - self.targetNode \
                                                        .getPosition()) \
                                                        * self.tightness
        self.targetNode.translate(displacement)
        """
        
        
    #def __del__(self):
        #self.cameraNode.detachAllObjects()
        #self.sceneManager.destroySceneNode(self.name)
        #self.sceneManager.destroySceneNode(self.name + "_target")
