import ogre.physics.bullet as bullet
import ogre.renderer.OGRE as Ogre
from MotionStates import OgreMotionState, NoRotOgreMotionState

class CollisionCallback(object):
    def bodiesCollided(self, body0, body1):
        pass


class PhysicsWorld(object):
    def __init__(self, sceneManager):
        self.objects = list() # tymczasowy cache
        self.mgr = sceneManager
        self.world = None
        self.callback = None
        self._body_bag = []

    def setCollisionCallback(self, callback):
        self.callback = callback
        
    """
    def buildStaticPlane(self, node, normal, constant=1):
        shape = bullet.btStaticPlaneShape(bullet.btVector3(*normal),constant);
        self.buildStaticObject(shape)
    
    def buildStaticObject(self, node, shape):
        if not self.world:
            raise Exception("No world yet")
        
        
        ms = bullet.btDefaultMotionState(bullet.btTransform(bullet.btQuaternion(0,0,0,1),bullet.btVector3(0,-1,0)));
        
        self.groundRigidBodyCI = bullet.btRigidBody.btRigidBodyConstructionInfo(0,self.groundMotionState,self.groundShape,bullet.btVector3(0,0,0));
        self.groundRigidBody = bullet.btRigidBody(self.groundRigidBodyCI)
        self.world.addRigidBody(self.groundRigidBody)
       """ 

    """
            entity = mgr.createEntity("fallbox", "cube.mesh")
            boundingB = entity.getBoundingBox()
            size = boundingB.getSize()
            size /= 2.0
            size *= 0.95
            self.fallShape = bullet.btBoxShape(bullet.btVector3(size.x, size.y, size.z))
            self.mass = 1
            self.fallInertia = bullet.btVector3(0,0,0);
            self.fallShape.calculateLocalInertia(self.mass,self.fallInertia);
    """     
    
    def buildShape(self, entity, primitive, mesh=None, size=None):
        if primitive == "box":
            boundingB = entity.getBoundingBox()
            if not size:
                size = boundingB.getSize()
            size /= 2.0
            size *= 0.95
            shape = bullet.btBoxShape(bullet.btVector3(size.x, size.y, size.z))
            self.objects.append(shape)
            return shape
        if primitive == "sphere":
            if not size:
                size = entity.getBoundingRadius()
            size *= 0.95
            shape = bullet.btSphereShape(size)
            self.objects.append(shape)
            return shape

        #sphere
        #capsule
        #convex_hull
        raise Exception("Unrecognised PHYS SHAPE: <%s>" % primitive)

    def buildMotionState(self, ms=None):
        pass
    
    # depricated!
    """def getBody4Node(self, node):
        if isinstance(node, Ogre.Entity):
            node = node.getParentNode()
        result = None
        while result == None and node <> None:
            result = self.nodeBodyMapping.get(node.getName(), None)
            node = node.getParent()
        return result"""

    def addSlider(self, body, args=(1.0, 0.0, 0.0), motor=False, velocity=1.0, maxImpulse=100):
        slider = bullet.btSliderConstraint(body, bullet.btTransform(
                                                    bullet.btQuaternion(*args[:4]),
                                                    bullet.btVector3(*args[4:])
                                                                  ), False)
        
        slider.setTargetLinMotorVelocity(velocity)
        slider.setMaxLinMotorForce(maxImpulse)
        slider.setPoweredLinMotor(motor)
        
        self.objects.append(slider)
        self.world.addConstraint(slider)
        return slider

    def addHinge(self, body, args, motor=False, velocity=1.0, maxImpulse=100):
        hinge = bullet.btHingeConstraint(body, bullet.btTransform(
                                                    bullet.btQuaternion(*args[:4]),
                                                    bullet.btVector3(*args[4:])
                                                                  ))
        hinge.enableAngularMotor(motor, velocity, maxImpulse)
        self.objects.append(hinge)
        self.world.addConstraint(hinge)
        return hinge

    def addHingeThrough(self, body, args, motor=False, velocity=1.0, maxImpulse=100):
        hinge = bullet.btHingeConstraint(body, bullet.btTransform(
                                                    bullet.btQuaternion(*args[:4]),
                                                    bullet.btVector3(*args[4:])
                                                                  ))
        hinge.enableAngularMotor(motor, velocity, maxImpulse)
        self.objects.append(hinge)
        self.world.addConstraint(hinge)
        return hinge
    
    def buildGhostBody(self, node, shape):
        pos = node.getPosition()
        rot = node.getOrientation()
        ms =  bullet.btDefaultMotionState(bullet.btTransform(bullet.btQuaternion(rot.x, rot.y, rot.z, rot.w),
                                                bullet.btVector3(pos.x,pos.y,pos.z)));
        #bodyCI = bullet.btRigidBody.btRigidBodyConstructionInfo(0,ms,shape,bullet.btVector3(0,0,0))
        body = bullet.btGhostObject()
        body.setCollisionShape(shape)
        #body.setWorldTransform((object)
        #powerup->setCollisionFlags(btCollisionObject::CF_KINEMATIC_OBJECT|btCollisionObject::CF_NO_CONTACT_RESPONSE);
        body.setCollisionFlags(bullet.btCollisionObject.CF_NO_CONTACT_RESPONSE)
        
        
        pos = node.getPosition()
        rot = node.getOrientation()
        body.setWorldTransform( bullet.btTransform(bullet.btQuaternion(rot.x, rot.y, rot.z, rot.w), bullet.btVector3(pos.x,pos.y,pos.z)) )
        self.world.addCollisionObject(body)
        self.objects.append(body)
        self.objects.append(ms)
        return body
    
    def buildStaticBody(self, node, shape, friction=0.5):
        pos = node.getPosition()
        rot = node.getOrientation()
        ms =  bullet.btDefaultMotionState(bullet.btTransform(bullet.btQuaternion(rot.x, rot.y, rot.z, rot.w),
                                                bullet.btVector3(pos.x,pos.y,pos.z)));
        bodyCI = bullet.btRigidBody.btRigidBodyConstructionInfo(0,ms,shape,bullet.btVector3(0,0,0))
        bodyCI.m_friction = friction
        body = bullet.btRigidBody(bodyCI)
        self.world.addRigidBody(body)
        self.objects.append(body)
        self.objects.append(ms)
        return body

    def buildRigidBody(self, node, shape, mass, ms=None, friction=0.5):  
        pos = node.getPosition()
        rot = node.getOrientation()
        if not ms:
            ms = OgreMotionState(bullet.btTransform(bullet.btQuaternion(rot.x, rot.y, rot.z, rot.w),
                                                bullet.btVector3(pos.x,pos.y,pos.z)),
                                                node)
        elif ms == "no_rotation":
            ms = NoRotOgreMotionState(bullet.btTransform(bullet.btQuaternion(rot.x, rot.y, rot.z, rot.w),
                                                bullet.btVector3(pos.x,pos.y,pos.z)),
                                                node)
        
        inertia = bullet.btVector3(0,0,0);
        shape.calculateLocalInertia(mass, inertia);
        bodyCI = bullet.btRigidBody.btRigidBodyConstructionInfo(mass,ms,shape,inertia)
        bodyCI.m_friction = friction
        body = bullet.btRigidBody(bodyCI)
        self.world.addRigidBody(body)
        self.objects.append(body)
        self.nodeBodyMapping[node.getName()] = body
        print "adding ", node.getName()
        self.objects.append(ms)
        return body
    
    
    """
    It is not allowed to add or remove objects during the collision callback.
    It is best to delay those deletions and perform them after the stepSimulation finished
    
    Na razie nie ma callbackow, tylko za kazdym razem przegladane sa manifoldy
    Przy przepisywaniu na callbacki poprawic
    """
    def removeBody(self, body):
        self.world.removeRigidBody(body)
    
    def destroyAll(self):
        del self.world
        del self.nodeBodyMapping
        

    def buildWorld(self):
        self.collisionConfiguration = bullet.get_btDefaultCollisionConfiguration()
        #broadphase = bullet.btDbvtBroadphase()
        self.broadphase = bullet.btAxisSweep3(bullet.btVector3(-10000,-10000,-10000), bullet.btVector3(10000,10000,10000), 1024)
        self.dispatcher = bullet.get_btCollisionDispatcher1(self.collisionConfiguration)
        self.solver = bullet.btSequentialImpulseConstraintSolver()
        self.world = bullet.btDiscreteDynamicsWorld(self.dispatcher, self.broadphase , self.solver, self.collisionConfiguration)
        self.world.setGravity(bullet.btVector3(0, 0.0, -10.0))
        #self.ghostPairCallback =  bullet.btGhostPairCallback();
        #self.world.getPairCache().getInternalGhostPairCallback(self.ghostPairCallback)
        self.nodeBodyMapping = dict()
        
        """
        self.groundMotionState = bullet.btDefaultMotionState(bullet.btTransform(bullet.btQuaternion(0,0,0,1),bullet.btVector3(0,-1,0)));
        
        self.groundRigidBodyCI = bullet.btRigidBody.btRigidBodyConstructionInfo(0,self.groundMotionState,self.groundShape,bullet.btVector3(0,0,0));
        self.groundRigidBody = bullet.btRigidBody(self.groundRigidBodyCI)
        self.world.addRigidBody(self.groundRigidBody)
        """
        
        
        #self.fallMotionState = bullet.btDefaultMotionState(bullet.btTransform(bullet.btQuaternion(0,0,0,1),bullet.btVector3(0,50,0)));
        
        #mass = bullet.btScalar(1);
        
        #self.fallShape = bullet.btSphereShape(1);
        """
        entity = self.mgr.createEntity("fallbox", "cube.mesh")
        boundingB = entity.getBoundingBox()
        size = boundingB.getSize()
        size /= 2.0
        size *= 0.95
        self.fallShape = bullet.btBoxShape(bullet.btVector3(size.x, size.y, size.z))
        
        
        node = self.mgr.getRootSceneNode().createChildSceneNode()
        node.attachObject(entity)
        self.buildRigidBody(node, self.fallShape, 1.0)
        
        
        self.world.stepSimulation(-0.33)
        self.world.stepSimulation(0.1)
        """
        
        #for x in range(100):
        #    self.world.stepSimulation(0.1)
        
        """self.objects.append(groundRigidBody)
        self.objects.append(fallRigidBody)
        
        self.objects.append(groundMotionState)
        self.objects.append(groundRigidBodyCI)
        self.objects.append(groundShape)
        self.objects.append(fallShape)
        self.objects.append(fallMotionState)
        self.objects.append(fallRigidBodyCI)
        #self.objects.append()
        #self.objects.append()"""
    
    def teleport(self, actor):
        actor.getBody().getWorldTransform().setIdentity();
        actor.getBody().getWorldTransform().setOrigin(bullet.btVector3(48,80,40));
        #self.world.removeRigidBody(actor.getBody())
        #self.world.stepSimulation(0.1)
        #transform = bullet.btTransform(bullet.btQuaternion(0,0,0,0), bullet.btVector3(48,80,40));
        #ms = OgreMotionState(transform, actor.getNode())
        #actor.getBody().setMotionState(ms)
        #self.objects.append(ms)
        #self.world.addRigidBody(actor.getBody())
        #actor.getBody().getWorldTransform().setOrigin(transform.getOrigin())
        #actor.getBody().activate(True)
        #actor.getNode().setPosition(transform.getOrigin().x(), transform.getOrigin().y(), transform.getOrigin().z())
        
    def timePassed(self, time):
        if time > 0.0:
         #   print time*100
        #pass
            self.world.stepSimulation(time)
            
            if self.callback:
                
                numManifolds = self.world.getDispatcher().getNumManifolds()
                
                for i in range(numManifolds): 
                    #btPersistentManifold* 
                    contactManifold =  self.world.getDispatcher().getManifoldByIndexInternal(i);
                    #btCollisionObject* ?
                    obA = contactManifold.getBodyAsObject0()
                    #btCollisionObject* ?
                    obB = contactManifold.getBodyAsObject1()
                    self.callback.bodiesCollided(obA, obB)
                # To jest obliczanie punktow styku (najprawdopodobniej maksymalnie 4)
                #int numContacts = contactManifold->getNumContacts();
                #for (int j=0;j<numContacts;j++)
                #{
                #    btManifoldPoint& pt = contactManifold->getContactPoint(j);
                #    if (pt.getDistance()<0.f)
                #    {
                #        const btVector3& ptA = pt.getPositionWorldOnA();
                #        const btVector3& ptB = pt.getPositionWorldOnB();
                #        const btVector3& normalOnB = pt.m_normalWorldOnB;
                #    }
                #}
            #}

