import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS
import SceneIOListener
from PCSkills import PCSkills

class CharacterSkillControl:
    def __init__(self, mTimeEventManager, keyboard, character=None):
        self.keyboard = keyboard
        self.setCharacter(character)
        mTimeEventManager.registerOnNewFrame(self)
    
    def onTimeEvent(self, *args):
        if not self.character: return
        
        if self.keyboard.isKeyDown(OIS.KC_H):
            self.character.useSkill(PCSkills.GENERIC_HEALING)
        
        
    def destroyAll(self):
        del self.character
    
    def setCharacter(self, character):
        self.character = character

