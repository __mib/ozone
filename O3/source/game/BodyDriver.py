import ogre.physics.bullet as bullet
import TimeEventManager
from CooldownDecorator import CooldownWard

class CentralForceBodyDriver(TimeEventManager.TimeEventListener):
    """ Uwaga: niektore funkcje wzajemnie sie wykluczaja """
    
    def destroyAll(self):
        self.timeMgr.unregisterOnNewFrame(self)
        del self.body
        del self.timeMgr
    
    @CooldownWard(1)
    def jump(self):
        self.body.activate()
        if self.jumpListener:
            self.jumpListener.onJump()
        self.body.applyCentralImpulse( bullet.btVector3(0, 0, 50.0) )
    
    def registerOnJumpListener(self, i):
        self.jumpListener = i
    
    def __init__(self, body, timeMgr):
        self.body = body   
        self.timeMgr = timeMgr
        timeMgr.registerOnNewFrame(self)
        self.force = bullet.btVector3(0, 0, 0)
        self.limit = None
        self.jumpListener = None
    
    def addVelocityCap(self, limit):
        self.limit = limit

    def getTimeEventManager(self):
        return self.timeMgr

    def onTimeEvent(self, arg, eventType='NO_TYPE'):    
        if self.limit:
            velocity = self.body.getLinearVelocity()
            speed = velocity.length()
            if speed > self.limit:
                velocity *= self.limit/speed;
                self.body.setLinearVelocity(velocity);
                return
        self.body.activate()
        self.body.applyCentralForce(self.force * 120.0)
        
    def setDirection(self, d, multi=60.0):
        self.force = self.ogre2bullet(d) * multi
    
    def ogre2bullet(self, impulse):
        return bullet.btVector3(impulse.x, impulse.y, impulse.z)
        
    def startMovement(self, direction):
        self.force += self.ogre2bullet(direction)
        
    def stopMovement(self, direction):
        self.force -= self.ogre2bullet(direction)
        
    def induceMovement(self, direction):
        impulse = direction * 10
        self.body.activate()
        self.body.applyCentralImpulse(bullet.btVector3(impulse.x, impulse.y, impulse.z))

class TorqueBodyDriver:
    def __init__(self, body):
        self.body = body
    
    def induceMovement(self, direction):
        impulse = direction * 300.0
        self.body.activate()
        self.body.applyTorqueImpulse(bullet.btVector3(impulse.x, impulse.y, impulse.z))
