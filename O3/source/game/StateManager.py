
'''
Powinna istniec jedna instancja tej klasy
'''
class StateManager(object):

    def __init__(self):
        self.statesList = list()
        
    def push(self, state):
        print "push state", state
        if len(self.statesList) > 0:
            self.statesList[-1].suspend()
        self.statesList.append(state)
        print "starting new state"
        state.start()
    
    def pop(self):
        print "<POP>"
        self.statesList.pop().stop()
        print "POP: state stopped"
        if len(self.statesList) > 0:
            print "POP: resuming state"
            print "Will resume from this trace"
            import inspect
            for x in inspect.stack():
                print x
            self.statesList[-1].resume()
            print "POP: state resumed"
        print "</POP>"
