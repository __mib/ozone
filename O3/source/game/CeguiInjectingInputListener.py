import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI

from CeguiUtils import convertButton
    
class CeguiInjectingInputListener(Ogre.FrameListener, OIS.MouseListener, OIS.KeyListener):
    def __init__(self):
        Ogre.FrameListener.__init__(self)
        OIS.MouseListener.__init__(self)
        OIS.KeyListener.__init__(self)

    def frameStarted(self, evt):
        self.keyboard.capture()
        self.mouse.capture()
        return True
    
    def focus(self):
        self.mouse.setEventCallback(self)
        self.keyboard.setEventCallback(self)
    
    def defocus(self):
        self.mouse.setEventCallback(None)
        self.keyboard.setEventCallback(None)
        
    
    def mouseMoved(self, evt):
        CEGUI.System.getSingleton().injectMouseMove(evt.get_state().X.rel, evt.get_state().Y.rel)
        return True
 
    def mousePressed(self, evt, bid):
        CEGUI.System.getSingleton().injectMouseButtonDown(convertButton(bid))
        return True
 
    def mouseReleased(self, evt, bid):
        CEGUI.System.getSingleton().injectMouseButtonUp(convertButton(bid))
        return True
 
    def keyPressed(self, evt):
        ceguiSystem = CEGUI.System.getSingleton()
        ceguiSystem.injectKeyDown(evt.key)
        ceguiSystem.injectChar(evt.text)
        return True
 
    def keyReleased(self, evt):
        CEGUI.System.getSingleton().injectKeyUp(evt.key)
        return True

    """Secondary - compatible"""
    def destroyAll(self):
        pass
