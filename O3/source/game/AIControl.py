import TimeEventManager
import ogre.renderer.OGRE as Ogre
from game.CooldownDecorator import CooldownWard

class AIStrategy:
    def update(self, dt):
        pass
    def destroyAll(self):
        print "WARNING: generic destroyAll on object", self
        print "WARNING cont'd: this could lead to memory leaks and duplicate scene instances"

class StraightToEnemy(AIStrategy):
    def __init__(self, myMovable, enemy=None, force=None):
        self.enemy = enemy
        self.movable = myMovable
        if not force:
            force = lambda e, i: (e - i).normalisedCopy()
        self.force = force
        #self.movable.getDriver().addVelocityCap(3.0)

    def update(self, dt):
        e = self.enemy.position
        i = self.movable.getNode().position
        self.movable.getDriver().setDirection( self.force(e, i), 1.0 )
   
    def destroyAll(self):
        del self.enemy
        del self.movable

class Shooter(AIStrategy):
    def __init__(self, shooterObj, radius=30):
        self.targets=[]
        self.obj = shooterObj
        self.pos = self.obj.getNode().getParent().convertLocalToWorldPosition(self.obj.getNode().getPosition())
        self.setRadius(radius)
        self.raySceneQuery = self.obj.sceneFactory.getSceneManager().createRayQuery(Ogre.Ray())
        self.raySceneQuery.setSortByDistance(True, 0)
    
    
    def isVisible(self, target): # czy widzi target
        #starget.showBoundingBox(True)
        box = target._getWorldAABB()
        corners = box.getAllCorners()
        t = target.getParent().convertLocalToWorldPosition(target.getPosition())
        for c in corners:
            c += t
        tpos = target.getParent().convertLocalToWorldPosition(target.getPosition())
        corners+=(tpos, )
        for x in corners:
            corners+=((tpos + x)/2, )
        
        self.pos = self.obj.getNode().convertLocalToWorldPosition(self.obj.weapons.getCurrent().getNode().getPosition())
        pos = target.getParent().convertLocalToWorldPosition(target.getPosition())
        pos = pos - self.pos
        self.obj.getNode().setDirection(pos.x, pos.y, pos.z, Ogre.Node.TransformSpace.TS_WORLD)
        
        #for x in corners:
        for y in corners:
            pos = y        
            #self.obj.getNode().setRotation(pos)
            self.raySceneQuery.setRay(Ogre.Ray(self.pos, pos - self.pos)) 
            #print "jestem w " + str(t.x) + " " + str(t.y) + " " + str(t.z)
            #print "ray z " + str(self.pos.x) + " " + str(self.pos.y) + " " + str(self.pos.z) + " do " + str(pos.x) + " " + str(pos.y) + " " + str(pos.z)
            result = self.raySceneQuery.execute()
            #print "RayQueryResult", result, "object count:", len(result)
            #for r in result:
            #    print str(r.movable.getParentSceneNode()) + " " + str(r.movable.getParentSceneNode().getName()) + " " + str(self.obj.weapons.getCurrent().getNode().getName()) 
            if len(result) > 1:
                q = 1
                while q < len(result) and result[q].movable.getParentSceneNode().getName() == self.obj.weapons.getCurrent().getNode().getName():
                    q = q + 1
                if q < len(result) and result[q].movable.getParentSceneNode().getName() == target.getName():
                    return pos
        return None
    
    def update(self, dt):
        dist = 100000
        target = None
        for t in self.targets:
            pos = t.getParent().convertLocalToWorldPosition(t.getPosition())
            odl = self.pos.squaredDistance(pos)
            #print str(odl) + " " + str(dist) + " " + str(self.radius) 
            if odl < self.radius and odl < dist:
                dist = odl
                target = self.isVisible(t)
        if target != None:
            self.obj.weapons.getCurrent().activate(target)
            #self.obj.getNode().setDirection(target.x, target.y, target.z)
            #print str(pos.x) + " " + str(pos.y) + " " + str(pos.z)
            
    def addTarget(self, target):
        self.targets.append(target)
    def removeTarget(self, target):
        self.targets.remove(target)
    def getTargets(self):
        return self.targets
    def setRadius(self, radius):
        self.radius = radius*radius        

class AIStrategyFactory:
    def __init__(self):
        pass
    
class AIManager(TimeEventManager.TimeEventListener):
    def __init__(self, mTimeEventManager):
        self.strategies = set()
        self.mTimeEventManager = mTimeEventManager
        mTimeEventManager.registerOnNewFrame(self)

    def getTimeEventManager(self):
        return self.mTimeEventManager

    def destroyAll(self):
        for x in self.strategies:
            x.destroyAll()
        del self.strategies
        del self.mTimeEventManager

    #@CooldownWard(0.2)
    def onTimeEvent(self, dt, eventType='NO_TYPE'):
        for s in self.strategies:
            s.update(dt)

    def registerStrategy(self, strategy):
        self.strategies.add(strategy)
        
    def removeStrategy(self, strategy):
        self.strategies.remove(strategy)
