import ogre.renderer.OGRE as Ogre
from xml.dom import minidom
import MotionStates

class SceneElementFactory:
    def __init__(self, sceneMgr):
        self.sceneMgr = sceneMgr
        print "dostalem aaa", sceneMgr
        print "name",  sceneMgr.getName()
        print "toja", self
    
    def getSceneManager(self):
        print "tu sef, zwracam scenemgr", self.sceneMgr
        m = self.sceneMgr
        print "mam go?"
        print m.getName()
        print "r"
        print "toja", self
        return m
    # 0x04AC1B70
    def makeNode(self, name, parent):
        node = self.sceneMgr.createSceneNode(name)
        parent.addChild(node)
        return node

    def makeEntity(self, name, meshname, groupname):
        return self.sceneMgr.createEntity(name, meshname, groupname)
    def createEntity(self, name):
        return self.sceneMgr.createEntity(name)
    def makeLight(self):
        return self.sceneMgr.createLight()

    def makeParticleSystem(self, name, templatename):
        return self.sceneMgr.createParticleSystem(name, templatename)

    def makeSphere(self, name, radius):
        sphere = self.sceneMgr.createEntity(name, Ogre.SceneManager.PT_SPHERE)
        scaleNode = self.sceneMgr.createSceneNode(name + "_scalenode")
        scaleNode.attachObject(sphere)
        scaleNode.setScale(radius, radius, radius)
        return scaleNode
    
    def destroyAll(self):
        print "FDESTROY"
        pass
        #del self.sceneMgr

class DSFormatException(Exception):
    pass

class DotSceneParser(object):

    def destroyAll(self):
        self.elementFactory.destroyAll()
        del self.elementFactory
        del self.sceneMgr
        del self.log
        del self.physWrapper
        del self.levelElementFactory
        del self.sceneChildDispatch
        del self.nodeChildDispatch
        del self.lightChildDispatch
        
    def handlerNotFound(self, node, parent):
        if node.nodeName <> '#text':
            self.log.logMessage("(warning) no handler for node %s" % node.nodeName, Ogre.LML_NORMAL)

    def getBackgroundColor(self):
        return self.colourBackground

    def __init__(self, name, resourceGroup, sceneMgr, physWrapper, sceneElementFactory, levelElementFactory, log, rootNode=None):
        self.resourceGroup = resourceGroup
        self.createDispatchTables()
        self.sceneMgr = sceneMgr
        self.colourBackground = None
        self.log = log
        self.physWrapper = physWrapper
        self.elementFactory = sceneElementFactory
        self.levelElementFactory = levelElementFactory
        #self.sceneMgr.setAmbientLight((0.5, 0.5, 0.5));
        self.rootNode = rootNode
        if not self.rootNode:
            self.rootNode = self.sceneMgr.getRootSceneNode()
        self.name = name
        
    def build(self):
        s = Ogre.ResourceGroupManager.getSingleton().openResource(self.name).getAsString()
    
        dom = minidom.parseString(s)
        self.scene = dom.getElementsByTagName("scene")[0]
        
        self.log.logMessage("Scene info", Ogre.LML_NORMAL)
        for i in range(self.scene.attributes.length):
            a = self.scene.attributes.item(i)
            self.log.logMessage("%s: %s" % (a.name, a.value), Ogre.LML_NORMAL)
        
        for x in self.scene.childNodes:
            self.sceneChildDispatch.get(x.nodeName, self.handlerNotFound)(x, self.scene)

    
    def parseNodes(self, node, parent):
        for x in node.childNodes:
            if x.nodeName == "node":
                self.parseNode(x, self.rootNode)
    
    def attrs2vec(self, node, keys):
        return [float(node.getAttribute(c)) for c in keys] 
    
        
    def parseEnvironment(self, node, parent):
        for x in node.childNodes:
            if x.nodeName == "colourAmbient":
                self.sceneMgr.setAmbientLight(self.attrs2vec(x, ("r","g","b")))
            elif x.nodeName == "colourBackground":
                self.colourBackground = self.attrs2vec(x, ("r","g","b"))
            else:
                self.log.logMessage("Ignored environment variable: %s" % (x.nodeName), Ogre.LML_NORMAL)
    
    def createDispatchTables(self):
        self.sceneChildDispatch = {
            "nodes": self.parseNodes,
            "environment": self.parseEnvironment,
        }
        self.nodeChildDispatch = {
            "position": self.setPosition,
            "rotation": self.setRotation,
            "scale": self.setScale,
            "node": self.parseNode,
            # "user_data": self.parseUserData, # wolane recznie
            "entity": self.addEntity,
            "light": self.addLight,
        }
        self.lightChildDispatch = {
            "colourDiffuse": self.setLightDiffuse,
            "colourSpecular": self.setSpecularColour,
            "lightAttenuation": self.setLightAttenuation,
        }
        #for k in self.nodeChildDispatch:
        #    self.lightChildDispatch[k] = self.nodeChildDispatch[k]
    
    def setPosition(self, node, parent):
        parent.translate(float(node.getAttribute("x")), 
                         float(node.getAttribute("y")),
                         float(node.getAttribute("z")))
    

    def setRotation(self, node, parent):
        #parent.setOrientation(self.attrs2vec(node, ("qz", "qy", "qx", "qw")))
        x = -1.0 * float(node.getAttribute("qz"))
        y = float(node.getAttribute("qy"))
        z = -1.0 * float(node.getAttribute("qx"))
        w = float(node.getAttribute("qw"))
        parent.setOrientation(x, y, z, w)

                    
    def setScale(self, node, parent):
        parent.setScale(float(node.getAttribute("x")), 
                         float(node.getAttribute("y")),
                         float(node.getAttribute("z")))
    
    def parseUserData(self, node, parent):
        pass
        #if node.getAttribute("name") == 'actorClass':
        #    self.levelElementFactory.registerSceneActor(parent, node.getAttribute('value'))
    
    def setLightDiffuse(self, node, light):
        light.setDiffuseColour(self.attrs2vec(node, ("r","g","b")))
    
    def setSpecularColour(self, node, light):
        light.setSpecularColour(self.attrs2vec(node, ("r","g","b")))
    
    def setLightAttenuation(self, node, light):
        range = light.getAttenuationRange()
        constant = light.getAttenuationConstant()
        linear = light.getAttenuationLinear()
        quadratic =light.getAttenuationQuadric()
        if node.hasAttribute("range"):
            range = float(node.getAttribute("range"))
        if node.hasAttribute("constant"):
            constant = float(node.getAttribute("constant"))
        if node.hasAttribute("quadratic"):
            quadratic = float(node.getAttribute("quadratic"))
        if node.hasAttribute("linear"):
            linear = float(node.getAttribute("linear"))
        light.setAttenuation (range, constant, linear, quadratic)

    def addLight(self, node, parent):
        light = self.elementFactory.makeLight()
        light.setType(Ogre.Light.LT_POINT)  # LT_POINT   LT_DIRECTIONAL LT_SPOTLIGHT 
        parent.attachObject(light)
    
    """
        Tworzy mesh lub prefaba, podpina do parenta i zwraca
    """
    def makeVisualEntity(self, node, parent):
        obj = None
        if node.hasAttribute("meshFile"):
            obj = self.elementFactory.makeEntity(str(node) + node.getAttribute("meshFile") + "_ent_" + self.resourceGroup,
                                            node.getAttribute("meshFile"),
                                            self.resourceGroup)
            #parent.attachObject(obj)
        """elif node.hasAttribute("prefab"):
            if node.getAttribute("prefab") == "PT_SPHERE":
                obj = self.elementFactory.makeSphere(str(node) + "SPHERE", float(node.getAttribute("radius")))
                size = float(node.getAttribute("radius"))
            parent.addChild(obj)"""
        return obj
    
    def getMotionStateCode(self, node):
        ms_string = self.getFromUserData(node, "motion_state")
        if ms_string == "no_rotation":
            return "no_rotation"
        return None
    
    def getBulletShape(self, node, visualObject, size=None):
        if node.hasAttribute("collisionPrim"):
            return self.physWrapper.buildShape(visualObject, node.getAttribute("collisionPrim"), size=size)
        return None
    
    def str2flist(self, s):
        return [ float(x) for x in s.split(' ') ]
    
    def addEntity(self, node, parent):
        mass = float(node.getAttribute("mass") or "0.0")                
        visualObject = self.makeVisualEntity(node, parent)
        
        if node.getAttribute('ghost') != 'True':
            parent.attachObject(visualObject)
        
        if not self.physWrapper:
            print "Warning: physWrapper not available, bullet specs will not be processed"
            return
        
        if node.getAttribute("physics_type") == "NO_COLLISION" or not node.hasAttribute("physics_type"):
            return
        
        motionState = self.getMotionStateCode(node.parentNode)
        shape = self.getBulletShape(node, visualObject)

        friction = float(self.getFromUserData(node.parentNode, 'friction') or '0.5')

        if node.getAttribute("physics_type") == "STATIC":
            if node.getAttribute('ghost') == 'True':
                body = self.physWrapper.buildGhostBody(parent, shape)
            else:
                body = self.physWrapper.buildStaticBody(parent, shape, friction=friction)
        elif node.getAttribute("physics_type") == "RIGID_BODY":
            body = self.physWrapper.buildRigidBody(parent, shape, mass, ms=motionState, friction=friction)
        else:
            self.log.logMessage("Unrecognised physics_type: %s" % node.getAttribute("physics_type"), Ogre.LML_NORMAL)
            return
    
        # Constraints
        if self.getFromUserData(node.parentNode, 'hinge') != None:
            if self.getFromUserData(node.parentNode, 'motor'):
                hinge = self.physWrapper.addHinge(body, self.str2flist(self.getFromUserData(node.parentNode, 'hinge')), True, 
                                                  *self.str2flist(self.getFromUserData(node.parentNode, 'motor')))
            else:
                hinge = self.physWrapper.addHinge(body, self.str2flist(self.getFromUserData(node.parentNode, 'hinge')))
            self.levelElementFactory.registerHinge(body, hinge)
        
        if self.getFromUserData(node.parentNode, 'slider') != None:
            if self.getFromUserData(node.parentNode, 'motor'):
                slider = self.physWrapper.addSlider(body, self.str2flist(self.getFromUserData(node.parentNode, 'slider')), True,
                                                  *self.str2flist(self.getFromUserData(node.parentNode, 'motor')))
            else:
                slider = self.physWrapper.addSlider(body, self.str2flist(self.getFromUserData(node.parentNode, 'slider')))
            self.levelElementFactory.registerSlider(body, slider)
                
        if body <> None:# and node.getAttribute("actor") == "True":
            self.levelElementFactory.registerActorsBody(parent, body)
        
    def getFromUserData(self, node, varname):
        for x in node.childNodes:
            if x.nodeName == "user_data" and x.getAttribute("name") == varname:
                return x.getAttribute('value')
        return None
        

    def delegateCreationToActorClass(self, node):
        if self.getFromUserData(node, 'actorClass'):
            return True
        return False


    def parseNode(self, node, parentEntity):
        if node.nodeName <> "node":
            raise DSFormatException("<node> expected, %s found" % (node.nodeName,))
        
        if self.delegateCreationToActorClass(node):
            self.levelElementFactory.delegateCreation(node, parentEntity, self.elementFactory, self)
        else:
            self.reallyParseNode(node, parentEntity)
    
    def reallyParseNode(self, node, parentEntity):
        obj = self.elementFactory.makeNode(node.getAttribute("name"), parentEntity)
        
        if self.getFromUserData(node, 'particle_system'):
            obj.attachObject( self.sceneMgr.createParticleSystem(node.getAttribute("name") + "_ps", self.getFromUserData(node, 'particle_system')))
        
        for x in node.childNodes:
            self.nodeChildDispatch.get(x.nodeName, self.handlerNotFound)(x, obj)    
        return obj

        
"""
    --------------------------------
"""

class MyLog(Ogre.LogListener):
    def __init__(self):
        # Creates a C++ log that will try and write to console and file
        Ogre.LogListener.__init__(self)
                 
    def messageLogged(self, message, level, debug, logName):
        # This should be called by Ogre instead of logging
        pass
        #print message

class mockSceneNode(object):
    def __init__(self):
        pass
    
    def addChild(self, param):
        return

    def translate(self, x, y, z):
        return

class mockSceneMgr(object):
    def __init__(self):
        pass

    def getRootSceneNode(self):
        return mockSceneNode()

    def createSceneNode(self, name):
        return mockSceneNode()

if __name__ == '__main__':
    root = Ogre.Root()
    cf = Ogre.ConfigFile()
    cf.load("resources.cfg")
    
        # Create the global log manager instance
    logMgr = Ogre.LogManager()
        # create the instance of our log listener
    myLog = MyLog()
        # create a "log"
    currentLog = Ogre.LogManager.getSingletonPtr().createLog("dummy.log" 
                                                    , True  # it's the default log
                                                    , False     # I don't want it sent to the debug window
                                                    , False     # it's a virtual log, so you need a listener :)
                                                    )  
        # register our listener
    #currentLog.addListener ( myLog )  
    
    seci = cf.getSectionIterator()
      
    while seci.hasMoreElements():
        secName = seci.peekNextKey()
        settings = seci.getNext()
        for item in settings:
            typeName = item.key
            archName = item.value
            Ogre.ResourceGroupManager.getSingleton().addResourceLocation(archName, typeName, secName)
    Ogre.ResourceGroupManager.getSingleton().initialiseAllResourceGroups()
    m = mockSceneMgr()
    P = DotSceneParser("scex.scene","",m)
