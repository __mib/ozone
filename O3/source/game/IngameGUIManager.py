import TimeEventManager

class IngameGUIManager(TimeEventManager.TimeEventListener):
    def __init__(self, sheet, windowManager):
        self.sheet = sheet
        self.windowManager = windowManager
        self.dbgText = windowManager.getWindow("si_InfoText")
        self.dialogText = windowManager.getWindow("si_DialogText")
        self.statsText = windowManager.getWindow("si_Stats")
        
        self.hp = 100
        self.en = 100
        self.noJumpFor = 100.0
        
        self.updateStats()
        self.displayText("Welcome, welcome", 5.0)

        
    def updateStats(self):
        jumpCooldown = 0.5 - self.noJumpFor
        if jumpCooldown < 0:
            jumpCooldown = 0
        jumpCooldown = int( 100 * jumpCooldown / 0.5 )
        color = 'FF00FF00' if jumpCooldown == 0 else 'FFFF0000'
        jumpString = "JMP [colour='%s']%d%%\n" % ( color, jumpCooldown )
        self.statsText.setText("HP %d\nEN %d\n" % (self.hp, self.en) + jumpString) 
    
    def setHealth(self, amount):
        self.hp = amount
        self.updateStats()
    
    def setEnergy(self, amount):
        self.en = amount
        self.updateStats()
    
    def addDialogLine(self, who, text, duration=None):
        print "DIALOG", who, text
        self.displayText(who + ": " + text, duration)
        #TODO
        
    def displayText(self, text, duration=None):
        self.dialogText.setText(text)
        self.dialogTextDuration = duration
    
    def setTimeEventManager(self, mTimeEventManager):
        self.mTimeEventManager = mTimeEventManager
        self.mTimeEventManager.registerOnNewFrame(self)
    
    def onJump(self):
        self.noJumpFor = 0
        self.updateStats()
    
    def onTimeEvent(self, arg, eventType='NO_TYPE'):
        self.noJumpFor += arg
        self.updateStats()
        if self.dialogTextDuration:
            self.dialogTextDuration -= arg
            if self.dialogTextDuration <= 0.0:
                self.displayText("")
                self.dialogTextDuration = None
            