import TimeEventManager
import ogre.renderer.OGRE as Ogre
from ogre.sound.ogreoggsound import OgreOggSoundManager

class SoundWrapper:
    def __init__(self, sound):
        self.sound = sound
    def stopPlaying(self):
        pass
    def setVolume(self,volume):
        self.sound.setVolume(volume)
    def getVolume(self):
        return self.sound.getVolume()

class SoundManagerListener(TimeEventManager.TimeEventListener):
    def __init__(self, mSceneManager, mListener):
        self.mSceneManager = mSceneManager
        
        print "wut"
        OgreOggSoundManager.getSingleton().init()
        OgreOggSoundManager.getSingleton().setSceneManager(self.mSceneManager)
        #OgreOggSoundManager.getSingleton().setDistanceModel(0xD003) #linear distance
        self.listener = mListener
        mListener.getParentSceneNode().attachObject(OgreOggSoundManager.getSingleton().getListener())
        self.sounds = dict()
        #self.sounds["blast"] = OgreOggSoundManager.getSingleton().createSound("countdown.wav","countdown.wav",loop=True)
    
    def destroyAll(self):
        if hasattr(self, "listener"):
            del self.listener
        if hasattr(self, "mSceneManager"):
            del self.mSceneManager
        print "@kait: nie wiem, czy tu trzeba jeszcze cos zrobic."
        
    def onTimeEvent(self, arg, dummy):
        #position =  OgreOggSoundManager.getSingleton().getListener().getPosition()
        #sound = OgreOggSoundManager.getSingleton().getSound("blast1")
        #if sound:
        #    print "Sound:", sound.getPosition()
        #print position, arg
        OgreOggSoundManager.getSingleton().update(arg)
    
    def addSoundToNode(self, node, soundname, loop=False, volume=1.0):
        name = soundname
        if loop:
            name += "loop"
        if not name in self.sounds:
            self.sounds[name] = 0
        else:
            self.sounds[name] += 1
        name += str(self.sounds[name])
        sound = OgreOggSoundManager.getSingleton().createSound(name,soundname,loop=loop)
        node.attachObject(sound)
        sound.play()
        wrapper = SoundWrapper(sound)
        wrapper.setVolume(volume)
        return wrapper