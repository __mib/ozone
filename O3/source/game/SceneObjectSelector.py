import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS

import SceneIOListener

"""
    Pozwala na zaznaczenie obiektu znajdujacego sie na srodku ekranu. Dla debugu.
"""

class SceneObjectSelectedEvent(object):
    def objectSelectedEvent(self, node):
        pass

class SceneObjectSelector(SceneIOListener.SecondaryIOListener):
    def __init__(self, cameraMan, sceneManager, log):
        self.camera = cameraMan.getCamera()
        self.active = False
        self.log = log
        self.raySceneQuery = sceneManager.createRayQuery(Ogre.Ray())
        self.raySceneQuery.setSortByDistance(True, 2)
        self.listener = None
        self.compositor = False
    
    def destroyAll(self):
        del self.camera
        del self.listener
    
    def activate(self):
        self.active = True
    
    def deactivate(self):
        self.active = False
        
    def setListener(self, listener):
        self.listener = listener
        
    def keyPressed(self, evt):
        if evt.key == OIS.KC_F1:
            self.active = not self.active
            if self.active:
                self.log.logMessage("Selector active", Ogre.LML_TRIVIAL)
            else:
                self.log.logMessage("Selector deactivated", Ogre.LML_TRIVIAL)
        if evt.key == OIS.KC_F2:
            #CS_NAME = "Sharpen Edges"
            #CS_NAME = "Tiling"
            #CS_NAME = "Old Movie"
            #CS_NAME = "ASCII"
            #CS_NAME = "Night Vision"
            #CS_NAME = "Old TV"            
            
            CS_NAME = "Old Movie"
            
            Ogre.CompositorManager.getSingleton().addCompositor(self.camera.getViewport(), CS_NAME)
            if self.compositor:
                Ogre.CompositorManager.getSingleton().setCompositorEnabled(self.camera.getViewport(),CS_NAME, False)
                self.compositor = False
            else:
                Ogre.CompositorManager.getSingleton().setCompositorEnabled(self.camera.getViewport(),CS_NAME, True)
                self.compositor = True
        if evt.key == OIS.KC_F3:
            root = Ogre.Root.getSingleton()
            print "OGRE INFOBOX"
            print "FPS stats smoothing:\t", root.getFrameSmoothingPeriod()
            #print "Internal OGRE clock:\t", root.getTimer()
            print "Next frame number:\t", root.getNextFrameNumber()
            rs = root.getRenderSystem()
            print "Current render system:\t", rs.getName()
            window = root.getAutoCreatedWindow()
            print "AutoWindow\t", window.getName()
            print "Last FPS\t", window.getLastFPS()
            print "Average FPS\t", window.getAverageFPS()
            print "Best FPS\t", window.getBestFPS()
            print "Worst FPS\t", window.getWorstFPS()
             
            
            
            
    def mousePressed(self, evt, bid):
        if self.active:
            mouseRay = self.camera.getCameraToViewportRay(0.5,0.5)
            self.raySceneQuery.setRay(mouseRay)
            x = self.raySceneQuery.execute()[1]
            print x, x.movable.getName(), x.distance
            if self.listener:
                self.listener.objectSelectedEvent(x.movable)