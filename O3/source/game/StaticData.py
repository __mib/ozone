import pickle
#Narazie jeden na cala gre, jesli beda usery to jeden per user
class StaticData:
    instance = None
    dataFile="../environ/static.data"
    
    def __init__(self):
        self.data = []
        try:
            file = open(StaticData.dataFile, "r")
            self.data = pickle.load(file)
            file.close()
        except:
            pass        
    
    def save(self):
        file = open(StaticData.dataFile, "w+")
        pickle.dump(self.data, file)
        file.close()
        
    def add(self, element):
        self.data.append(element)
    
    def isSatisfied(self, list):
        for i in list:
            if i not in self.data:
                return False
        return True
    
    @staticmethod
    def getInstance():
        if StaticData.instance == None:
            StaticData.instance = StaticData()
        return StaticData.instance
