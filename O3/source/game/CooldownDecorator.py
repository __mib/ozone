"""
    Ten dekorator stosuje sie tylko do metod klas!
    Klasa musi miec zdefiniowana metode getTimeEventManager
"""
import game.TimeEventManager

class CooldownWard:
    def __init__(self, timeout):
        self.timeout = timeout

    def __call__(self, f):
        ward_name = '_cw__' + f.__name__
        timeout = self.timeout
            
        def wrapped(o, *args, **kwargs):
            currentTimeStamp = o.getTimeEventManager().getTimestamp()
            if getattr(o, ward_name, 0.0) > currentTimeStamp:
                return
            
            setattr(o, ward_name, currentTimeStamp + timeout)
            return f(o, *args, **kwargs)
            
        return wrapped

