import ogre.renderer.OGRE as Ogre

class TimeEventListener(object):
    def onTimeEvent(self, arg, eventType='NO_TYPE'):
        pass
    def destroyAll(self):
        print "WARNING: generic destroyAll on object", self
        print "WARNING cont'd: this could lead to memory leaks and duplicate scene instances"

class RefTEListener(TimeEventListener):
    def __init__(self, method, object_instance):
        self.method = method
        self.arg = object_instance
    
    def onTimeEvent(self, arg, et=None):
        self.method(self.arg)
        del self.arg
        del self.method

class TimeEventManager(object):
    def __init__(self, log):
        self.timeStamp = 0.0
        self.onNewFrame = list()
        self.log = log
        self.onTimeEvents= set()
    
    ETYPE_NF = 'NF'
    ETYPE_OT = 'OT'
    
    def destroyAll(self):
        for x in self.onNewFrame:
            x.destroyAll()
    
    def getTimestamp(self):
        return self.timeStamp
    
    def resetTimestamp(self):
        self.timeStamp = 0.0
    
    def timePassed(self, time):
        self.timeStamp += time
        for callback in self.onNewFrame:
            callback.onTimeEvent(time, self.ETYPE_NF)
            
        execution_schedule = list()
        
        for ts, callback in self.onTimeEvents:
            if ts <= self.timeStamp:
                execution_schedule.append( (ts, callback) )
        
        for (ts, callback) in execution_schedule:
            self.onTimeEvents.remove( (ts, callback) )
            callback.onTimeEvent(self.timeStamp, self.ETYPE_OT)
            
    
    def registerCallbackOn(self, callback, future_timestamp):
        self.onTimeEvents.add( (future_timestamp, callback) )
    
    def registerCallbackIn(self, callback, time_to_pass):
        self.registerCallbackOn(callback, self.timeStamp + time_to_pass)
        return self.timeStamp + time_to_pass
    
    def unregisterOnNewFrame(self, callback):
        self.onNewFrame.remove(callback)
    
    def registerOnNewFrame(self, callback):
        if not isinstance(callback, TimeEventListener):
            self.log.logMessage("Attempt to register non-TimeEventListener object", Ogre.LML_CRITICAL)
        #    return False
        self.onNewFrame.append(callback)
        return True
