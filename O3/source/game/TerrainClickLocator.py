import ogre.renderer.OGRE as Ogre
import ogre.gui.CEGUI as CEGUI
from ogre.sound.ogreoggsound import OgreOggSoundManager
from SceneIOListener import SecondaryIOListener
import time


"""
    Generuje eventy typu: uzytkownik kliknal i wypada to w 3D w punkcie XYZ
    Zastosowania: strzelanie
"""

class TerrainClickListener:
    def onClick(self, vector3d):
        pass
    def onMove(self, vector3d):
        pass
    def isActive(self):
        return True
    def destroyAll(self):
        print "WARNING: generic destroyAll on object", self
        print "WARNING cont'd: this could lead to memory leaks and duplicate scene instances"

class SampleTerrainClickLocatorListener(TerrainClickListener):
    """
        W miejscu w ktorym kliknal uzytkownik spawnuje mala kulke
    """

    def destroyAll(self):
        del self.mSceneManager
        del self.mSoundManager

    def __init__(self, mSceneManager, mSoundManager):
        self.mSceneManager = mSceneManager
        self.mSoundManager = mSoundManager #to w przyszlosci zniknie
        
        self.expcount = 0
        #self.sound.disable3D(True)
        #self.mSceneManager.getCamera("DotSceneCamera").getParentSceneNode().attachObject(OgreOggSoundManager.getSingleton().getListener());
        
    def onClick(self, location):
        """print "SampleTerrainClickLocatorListener: spawning marker at", location
        marker = self.mSceneManager.createEntity(Ogre.SceneManager.PT_SPHERE)
        node = self.mSceneManager.getRootSceneNode().createChildSceneNode()
        self.expcount += 1
        explosion = self.mSceneManager.createParticleSystem("Explosion"+str(self.expcount)+str(marker), "Explosion")
        node.attachObject(marker)
        node.setPosition(location)
        node.setScale(0.03, 0.03, 0.03)
        self.mSoundManager.addSoundToNode(node, "countdown.wav")
        node.attachObject(explosion)"""

class TerrainClickLocator(SecondaryIOListener):
    def __init__(self, cameraMan, sceneManager, window):
        self.listeners = []
        self.cameraMan = cameraMan
        self.window = window
        self.raySceneQuery = sceneManager.createRayQuery(Ogre.Ray())
        self.raySceneQuery.setSortByDistance(True, 2)
        
    def registerListener(self, mTerrainClickListener):
        self.listeners.append(mTerrainClickListener)

    def destroyAll(self):
        for x in self.listeners:
            x.destroyAll()
        del self.listeners
        del self.cameraMan
        del self.window

    def mousePressed(self, evt, bid):
       # if bid <> OIS.
        
        if self.cameraMan.getCameraType() == "indirect":
            
            mousePos = CEGUI.MouseCursor.getSingleton().getPosition()
            w, h = self.window.getWidth(), self.window.getHeight()
            #print mousePos.d_x, mousePos.d_y, w, h
            x, y = mousePos.d_x/float(w), mousePos.d_y/float(h)
            #print "TerrainClickLocator mousePressed", x, y
            
            mouseRay = self.cameraMan.getCamera().getCameraToViewportRay(x, y)
            #mouseRay = self.cameraMan.getCamera().getCameraToViewportRay(0.5, 0.5)
            self.raySceneQuery.setRay(mouseRay)
            result = self.raySceneQuery.execute()
            #print "RayQueryResult", result, "object count:", len(result)
            #for r in result:
            #    print r.movable
            
            if len(result) < 2:
            #    print "za malo"
                return
            
            object_hit = self.raySceneQuery.execute()[1]
            print "TerrainClickLocator: hit object", object_hit.movable, "distance", object_hit.distance
            point_hit = ( mouseRay.getDirection() * object_hit.distance ) + mouseRay.getOrigin()
            #print "point hit", point_hit
            
            # object_hit.movable.showBoundingBox(True)
            
            print "calling listener"
        else:
            point_hit = self.cameraMan.getCamera().getDerivedPosition() + self.cameraMan.getCamera().getDerivedDirection()*50
            print "direct"
            print point_hit
        
        for listener in self.listeners:
            listener.onClick(point_hit) 

    def mouseMoved(self, evt):
        if self.cameraMan.getCameraType() == "indirect":
            mousePos = CEGUI.MouseCursor.getSingleton().getPosition()
            w, h = self.window.getWidth(), self.window.getHeight()
            #print mousePos.d_x, mousePos.d_y, w, h
            x, y = mousePos.d_x/float(w), mousePos.d_y/float(h)
            #print "TerrainClickLocator mousePressed", x, y
            
            mouseRay = self.cameraMan.getCamera().getCameraToViewportRay(x, y)
            self.raySceneQuery.setRay(mouseRay)
            result = self.raySceneQuery.execute()
            #print "RayQueryResult", result, "object count:", len(result)
            #for r in result:
            #    print r.movable
            
            if len(result) < 2:
            #    print "za malo"
                return
               
            object_hit = self.raySceneQuery.execute()[1]
            #print "TerrainClickLocator: hit object", object_hit.movable, "distance", object_hit.distance
            point_hit = ( mouseRay.getDirection() * object_hit.distance ) + mouseRay.getOrigin()
            # print "point hit", point_hit
        else:
            point_hit = self.cameraMan.getCamera().getDerivedPosition() + self.cameraMan.getCamera().getDerivedDirection()*50
            print "direct"
            print point_hit
        
        for listener in self.listeners:
            listener.onMove(point_hit)