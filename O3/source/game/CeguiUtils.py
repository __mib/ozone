import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI

def convertButton(oisID):
    if oisID == OIS.MB_Left:
        return CEGUI.LeftButton
    elif oisID == OIS.MB_Right:
        return CEGUI.RightButton
    elif oisID == OIS.MB_Middle:
        return CEGUI.MiddleButton
    else:
        return CEGUI.LeftButton
