import ogre.renderer.OGRE as Ogre

class SceneFrameListener(Ogre.FrameListener):
 
    def __init__(self, renderWindow, camera, keyboard, mouse, sceneManager, physics, timeEventManager, mStateManager):    
        Ogre.FrameListener.__init__(self)
        self.keyboard = keyboard
        self.mouse = mouse
        self.inputEvents = None
        self.mStateManager = mStateManager
        #self.updaters = set()
        self.physics = physics
        self.camera = camera
        self.timeEventManager = timeEventManager
        self.apocalypse_now = False
        #self.raySceneQuery = sceneManager.createRayQuery(Ogre.Ray())
        #self.text = ""
    
    def scheduleWorldsEnd(self):
        self.apocalypse_now = True
    
    #framerenderqueued
        
    def frameStarted(self, evt):
        if self.apocalypse_now:
            del self.physics
            del self.camera
            del self.timeEventManager
            self.mStateManager.pop()
            import gc
            print "sceneframelistener: pop exits"
            # nie konczymy programu, tylko sciagamy scene state
            print "Referrers to this LFS"
            for x in gc.get_referrers(self):
                print x
            return True
        
        if self.keyboard:
            self.keyboard.capture()
        if self.mouse:
            self.mouse.capture()
        #    print "capt"
        
        #evt.timeSinceLastFrame /= 10.0
        
        if self.inputEvents:
            self.inputEvents.timePassed(evt.timeSinceLastFrame)
 
        if self.physics:
            self.physics.timePassed(evt.timeSinceLastFrame)
 
        #        btTransform trans;
        #        fallRigidBody->getMotionState()->getWorldTransform(trans);
 
        #        std::cout << "sphere height: " << trans.getOrigin().getY() << std::endl
 
        self.timeEventManager.timePassed(evt.timeSinceLastFrame)
 
       
        
        #mouseRay = self.camera.getCameraToViewportRay(0.5,0.5)
        #self.raySceneQuery.setRay(mouseRay)
        # Execute query
        #result = self.raySceneQuery.execute()
        #self.text = ""
        #for item in result:
        #    self.text += item.movable.getName() + "\n" + str(item.distance) + "\n"
        #self.state.dbgText.setText(self.text)
            
        return True