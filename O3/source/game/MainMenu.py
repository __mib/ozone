import ogre.gui.CEGUI as CEGUI
import ogre.renderer.OGRE as Ogre
import GameState

class MainMenu(GameState.GameState):
    def quit(self, evt):
        exit() # to nie powinno tak wygladac
    
    def lauchDebugState(self, evt):
        self.stateManager.push(self.stateFactory.newDotSceneState("scex.scene", "SampleLevel", "SampleScene"))

    def dbgLevelSelected(self, evt):
        o = self.dLevelSelect.getFirstSelectedItem()
        if not o:
            return
        text = str(o.getText())
        scene, lvlClass, res = self.dLevelMap[text]
        self.stateManager.push(self.stateFactory.newDotSceneState(scene, lvlClass, res)) # cheat, lvlClass to tez nazwa resourceGroup
         
    def prepareLevelSelect(self):
        dLevelSelect = CEGUI.WindowManager.getSingleton().getWindow("dLevelSelect")
        self.dLevelSelect = dLevelSelect
        
        dbgLevels = (
            ( "Tutorial", "tutorial.scene", "TutorialLevel", "TutorialScene" ),
            ( "Lost in the New Real", "japa.scene", "SampleLevel", "JapaScene" ),
            ( "Leaf on the Wind", "mib3.scene", "Mib3Level", "Mib3Scene" ),
            ( "Condition: Coverage", "mib2.scene", "Mib2Level", "Mib2Scene" ),
            ( "Enter the Eye", "kaitscene.scene", "SampleLevel", "KaitScene" ),
            ( "The Beacon", "domino.scene", "SampleLevel", "DominoScene" ),
            ( "mib1", "scex.scene", "SampleLevel", "SampleScene" ),
            ( "dawid1", "scex.scene", "SampleLevel", "DawidScene" )
        )
        
        self.dLevelSelectEntries = [
            CEGUI.ListboxTextItem(x) for x,y,z,w in dbgLevels
        ]
        
        self.dLevelMap = dict( (x, (y, z, w)) for x, y, z, w in dbgLevels )
        
        for e in self.dLevelSelectEntries:
            e.setSelectionBrushImage("TaharezLook", "MultiListSelectionBrush")
        
        dLevelSelect.subscribeEvent(
            CEGUI.Listbox.EventSelectionChanged, self, 'dbgLevelSelected'
        )
        
        for e in self.dLevelSelectEntries:
            dLevelSelect.addItem(e)
    
    def init(self):
        self.sceneManager = self.root.createSceneManager(Ogre.ST_GENERIC, "Default SceneManager")
        self.camera = self.sceneManager.createCamera("MainMenuCamera")
        
        self.sheet = CEGUI.WindowManager.getSingleton().loadWindowLayout("o3MainMenu.layout")
        CEGUI.WindowManager.getSingleton().getWindow("ExitButton").subscribeEvent(CEGUI.PushButton.EventClicked, self, "quit")
        """       windowManager = CEGUI.WindowManager.getSingleton()
       quitButton = windowManager.getWindow("CEGUIDemo/QuitButton").subscribeEvent(CEGUI.PushButton.EventClicked, self, "quit")
Now that we have a pointer to the button, we now will subscribe to the clicked event. Every widget in CEGUI has a set of events that it supports, and they all begin with "Event". Here is the code to subscribe to the event:
       quitButton.subscribeEvent(CEGUI.PushButton.EventClicked, self, "quit")"""
        #CEGUI.WindowManager.getSingleton().getWindow("diagLoad").subscribeEvent(CEGUI.PushButton.EventClicked, self, "lauchDebugState")

        self.prepareLevelSelect()
        #dLevelSelect.addItem(CEGUI.ListboxTextItem("aaaaa"))
        #dLevelSelect.addItem(CEGUI.ListboxTextItem("bbbbb"))
        
    
        
    def resume(self):
        self.ceguiSystem.setGUISheet(self.sheet)
        self.viewport = self.window.addViewport(self.camera)
        self.root.addFrameListener(self.frameListener)
        self.ilistener.focus()
    
    def suspend(self):
        self.ceguiSystem.setGUISheet(None)
        print "menu suspending"
        self.root.removeFrameListener(self.frameListener)
        self.window.removeAllViewports()
        self.ilistener.defocus()
