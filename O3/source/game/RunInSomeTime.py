import TimeEventManager

class RunMethodInSomeTime(TimeEventManager.TimeEventListener):
    def __init__(self, delay, mTimeEventManager, method, method_args, method_kwargs, active_at_startup=True):
        self.method = method
        self.method_args = method_args
        self.method_kwargs = method_kwargs
        self.mTimeEventManager = mTimeEventManager
        if active_at_startup:
            self.eventTime = self.mTimeEventManager.registerCallbackIn(delay, self)
    
    """
        delay liczone jest do current_timestamp, nie od ostatnio ustawionego czasu
    """
    def reschedule(self, delay):
        self.eventTime = self.mTimeEventManager.registerCallbackIn(self, delay)
    
    def onTimeEvent(self, current_time, et=None):
        if self.eventTime > current_time:
            return
        self.method(*self.method_args, **self.method_kwargs)

    def destroyAll(self):
        del self.method
        del self.method_args
        del self.method_kwargs
        del self.mTimeEventManager