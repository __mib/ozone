import ogre.renderer.OGRE as Ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI
from SoundManager import SoundManagerListener
import AIControl
import MainMenu
import CameraMan
import PhysicsWrapper
from SceneFrameListener import SceneFrameListener
import DotSceneParser
import GameState
import SceneIOListener
import TimeEventManager
import Level
import SceneObjectSelector
from WSADCharacterControl import WSADCharacterControl
from CeguiInjectingInputListener import CeguiInjectingInputListener
import TerrainClickLocator
import sys

    
class TrivialFrameListener(Ogre.FrameListener):
    def frameStarted(self, evt):
        return True    

"""
Przed utworzeniem obiektu nalezy upewnic sie, ze potrzebne resource sa zaladowana
"""
class StateFactory(object):

    def __init__(self, root, window, manager, ceguiSystem, inputManager, mouse, keyboard, log):
        self.root = root
        self.window = window
        self.manager = manager
        self.ceguiSystem = ceguiSystem
        self.inputManager = inputManager
        self.mouse = mouse
        self.keyboard = keyboard
        self.log = log
    
    def newMainMenuInstance(self):
        listener = CeguiInjectingInputListener()
        listener.mouse = self.mouse
        listener.keyboard = self.keyboard
        
        state = MainMenu.MainMenu(self.manager)
        state.root = self.root
        state.window = self.window
        state.ceguiSystem = self.ceguiSystem
        state.frameListener = listener
        state.ilistener = listener
        state.stateManager = self.manager
        state.stateFactory = self
        state.init()
        return state
    
    def newDotSceneState(self, scene, lvlClass, resource_group):
        print "NEWDOT"
        sceneManager = self.root.createSceneManager(Ogre.ST_GENERIC, "SampleSceneManager")
        print "scenemanager", sceneManager.getName()
        # ponizsze linijki musza byc BEZPOSREDNIO po utworzeniu sceneManagera
        sceneManager.setShadowTechnique(Ogre.SHADOWTYPE_STENCIL_ADDITIVE)
        sceneManager.setShadowColour(Ogre.ColourValue(0.9, 0.9, 0.9))
        
        ioListener = SceneIOListener.SceneIOListener(self.mouse, self.keyboard, self.log)
        ioListener.focus()
        
        ceguiListener = CeguiInjectingInputListener()
        ioListener.registerSecondary(ceguiListener)
        
        timeEventMgr = TimeEventManager.TimeEventManager(self.log)
        
        camera = sceneManager.createCamera("DotSceneCamera")
        camera.setNearClipDistance(2)
        
        cameraMan = CameraMan.CameraMan("dotscenecameraman", sceneManager, camera)
        cmListener = CameraMan.SampleCameraManInputListener(cameraMan, ioListener)
        ioListener.registerSecondary(cmListener)
        
        timeEventMgr.registerOnNewFrame(cmListener)
        soundManager = SoundManagerListener(sceneManager, camera)
        timeEventMgr.registerOnNewFrame(soundManager)
                
        physics = PhysicsWrapper.PhysicsWorld(sceneManager)
        physics.buildWorld()
        
        
        frameListener = SceneFrameListener(self.window, camera, self.keyboard, self.mouse, sceneManager, physics, timeEventMgr, self.manager)
        

        mAIManager = AIControl.AIManager(timeEventMgr)
        mTerrainClickLocator = TerrainClickLocator.TerrainClickLocator(cameraMan, sceneManager, self.window)
        
        
        fullclassname = "levels." + lvlClass
        __import__(fullclassname)
        module = sys.modules[fullclassname]
        level = module.__dict__[lvlClass](sceneManager, cameraMan, ioListener)
        #eval("__import__(levels.%s)" % lvlClass)
        #eval("level = levels.%s(sceneManager, cameraMan, ioListener)" % lvlClass)
        #from levels.SampleLevel import SampleLevel
        #level = SampleLevel(sceneManager, cameraMan, ioListener)
        
        
        levelFac = Level.LevelElementFactory(level, sceneManager, mAIManager, mTerrainClickLocator)
        
        dsSceneElementFactory = DotSceneParser.SceneElementFactory(sceneManager)
        dsParser = DotSceneParser.DotSceneParser(scene, resource_group, sceneManager, physics, dsSceneElementFactory, levelFac, self.log)
        
        charControl = WSADCharacterControl(ioListener,cameraMan)
        ioListener.registerSecondary(charControl)

        selLis = levelFac.buildControlSelectionListener(charControl)
        level.charControl = charControl
        level.setSoundManager(soundManager)
        colDisp = Level.CollisionDispatcher(level)
        physics.setCollisionCallback(colDisp)
        
        level.setMainIOListener(ioListener)
        level.setWorld(physics)
        level.setTimeEventManager(timeEventMgr)
        level.mStateManager = self.manager
        level.camera = camera
        if level.getSettings().get('selectorAvailable', False):
            selector = SceneObjectSelector.SceneObjectSelector(cameraMan, sceneManager, self.log)
            selector.setListener(selLis)
            ioListener.registerSecondary(selector) 
       
        ioListener.registerSecondary(mTerrainClickLocator)
        mSampleTerrainClickLocatorListener = TerrainClickLocator.SampleTerrainClickLocatorListener(sceneManager, soundManager)
        mTerrainClickLocator.registerListener(mSampleTerrainClickLocatorListener)

        if 'skyPlane' in level.getSettings():
            plane = Ogre.Plane((0, -1, 0), -5000)
            sceneManager.setSkyPlane(True, plane, level.getSettings()['skyPlane'], 100, 1)
        
        dsParser.build()
        
        
        state = GameState.DotScene(self.manager, 
                                   self.root, 
                                   self.window, 
                                   self.ceguiSystem, 
                                   cameraMan,
                                   dsSceneElementFactory,
                                   ioListener,
                                   timeEventMgr,
                                   dsParser,
                                   physics,
                                   frameListener,
                                   level,
                                   charControl,
                                   soundManager
                                   )
        
        state.init()
        return state
