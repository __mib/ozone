from game.Level import Weapon, Bullet, Actor, Mortal
from game.BodyDriver import CentralForceBodyDriver
import ogre.gui.CEGUI as CEGUI
import ogre.renderer.OGRE as Ogre
from game.CooldownDecorator import CooldownWard
import game.TimeEventManager

class Missile(Actor):
    def collisionEvent(self, actor2):
        if isinstance(Mortal, actor2):
            actor2.onDamageAbsorbed(40)

class TennisBallThrower(Weapon):
    
    
    def activate(self):
        pass

class BallLauncher(Weapon):
    name = "BallLauncher"
    #cooldown = 300000 # microseconds
    force = 1000.0 # N
    mass = 19.0 # kg
    max_ammunition = 500
    shortcut = 9 # pod ktorym numerkiem
    
    def __init__(self, xml, parent, sceneFactory, levelFactory, parser):
        self.ammo = 0
        parentEntity = parent
        Weapon.counter = Weapon.counter + 1
        obj = parser.elementFactory.makeNode(self.name+str(Weapon.counter), parentEntity)
        levelFactory.registerSceneActor(obj, self)
        self.node = obj
        self.levelFactory = levelFactory
        self.parser = parser
        self.sceneFactory = sceneFactory
        self.xml = xml
        self.entity = self.parser.elementFactory.createEntity(Ogre.SceneManager.PT_SPHERE)
        self.node.attachObject(self.entity)
        shift = Ogre.Vector3(0,0,-5)
        self.getNode().setPosition(shift) 
        self.node.setScale(0.005,0.005,0.01)
        
    def pump(self):
        self.getLevel().getSoundManager().addSoundToNode(self.getNode(), "pump.wav", loop=False, volume=10.0)
    
    @CooldownWard(3)
    def activate(self, target):
        print "ammo: " + str(self.ammo) 
        #if self.is_usable() :
        if True:
            self.use()
            
            self.getLevel().getSoundManager().addSoundToNode(self.getNode(), "power.wav", loop=False, volume=10.0)
            self.getTimeEventManager().registerCallbackIn( game.TimeEventManager.RefTEListener(BallLauncher.pump, self), 0.4 )
            rootnode = self.sceneFactory.getSceneManager().getRootSceneNode()
            bullet = Ball(self.xml, rootnode, self.sceneFactory, self.levelFactory, self.parser)
            if self.getNode().getPosition() == None:
                return
            target = target - self.getNode().convertLocalToWorldPosition(self.getNode().getPosition())#self.getNode().convertWorldToLocalPosition(target) - shift
            bullet.getNode().setPosition(self.getNode().convertLocalToWorldPosition(self.getNode().getPosition()))# + shift)
            
            phys = self.levelFactory.getLevel().getWorld()
            bulletShape = phys.buildShape(bullet.getEntity(), "sphere")
            body = phys.buildRigidBody(bullet.getNode(), bulletShape, bullet.mass)
            body.setCcdMotionThreshold(0.2)
            body.setCcdSweptSphereRadius(0.001)
            bd = CentralForceBodyDriver(body, self.getLevelFactory().getLevel().getTimeEventManager())
            #print "target: " + str(target.x) + ", " + str(target.y) + ", " + str(target.z)
            target.normalise()
            #print "target: " + str(target.x) + ", " + str(target.y) + ", " + str(target.z)
            bd.induceMovement(target*Ogre.Vector3(300,300,300))
            bullet.setBody(body)
            self.levelFactory.registerActorsBody(bullet.getNode(), body)
            self.getTimeEventManager().registerCallbackIn( game.TimeEventManager.RefTEListener(Ball.onDeath, bullet), 3 )
            # stworz pocisk i go poslij
            bullet.driver = bd
            
             
class Ball(Bullet):
    name = "Ball"
    damage = 0.01
    mass = 10 # kg
    
    def __init__(self, xml, parent, sceneFactory, levelFactory, parser):
        parentEntity = parent
        Bullet.counter = Bullet.counter + 1
        obj = parser.elementFactory.makeNode(self.name+str(Bullet.counter), parentEntity)
        trace = parser.elementFactory.makeParticleSystem("Trace"+str(obj), "Trace")
        levelFactory.registerSceneActor(obj, self)
        self.node = obj
        self.levelFactory = levelFactory
        self.parser = parser
        self.sceneFactory = sceneFactory
        self.xml = xml
        self.entity = self.parser.elementFactory.createEntity(Ogre.SceneManager.PT_SPHERE)
        self.node.attachObject(self.entity)
        self.node.attachObject(trace)
        self.trace = trace
        self.node.setScale(0.07, 0.07, 0.07)
        
    def onDeath(self):
        self.trace.clear()
        self.trace.setEmitting(False)
        self.driver.destroyAll()
        self.getLevelFactory().removeActor(self)
        print "TRACE", self.trace.isInScene()
            
class Minigun(Weapon):
    name = "Minigun"
    #cooldown = 300000 # microseconds
    force = 100.0 # N
    mass = 19.0 # kg
    max_ammunition = 500
    shortcut = 2 # pod ktorym numerkiem
    
    def __init__(self, xml, parent, sceneFactory, levelFactory, parser):
        self.ammo = 0
        parentEntity = parent
        Weapon.counter = Weapon.counter + 1
        obj = parser.elementFactory.makeNode("Minigun"+str(Weapon.counter), parentEntity)
        levelFactory.registerSceneActor(obj, self)
        self.node = obj
        self.levelFactory = levelFactory
        self.parser = parser
        self.sceneFactory = sceneFactory
        self.xml = xml
        self.entity = self.parser.elementFactory.createEntity(Ogre.SceneManager.PT_SPHERE)
        self.node.attachObject(self.entity)
        shift = Ogre.Vector3(0,0,-2)
        self.getNode().setPosition(shift) 
        self.node.setScale(0.005,0.005,0.01)
        
    @CooldownWard(0.7)
    def activate(self, target):
        print "ammo: " + str(self.ammo) 
        #if self.is_usable() :
        if True:
            self.use()
            
            self.getLevel().getSoundManager().addSoundToNode(self.getNode(), "skill_hit.wav", loop=False, volume=10.0)
            
            
            #print "spawn"
            rootnode = self.sceneFactory.getSceneManager().getRootSceneNode()
            #print "mam roota", rootnode
            bullet = B_308Winchester(self.xml, rootnode, self.sceneFactory, self.levelFactory, self.parser)
            #print "a moze tu"
            if self.getNode().getPosition() == None:
                return
            target = target - self.getNode().convertLocalToWorldPosition(self.getNode().getPosition())#self.getNode().convertWorldToLocalPosition(target) - shift
            bullet.getNode().setPosition(self.getNode().convertLocalToWorldPosition(self.getNode().getPosition()))# + shift)
            
            #weaponp = self.getNode().convertLocalToWorldPosition(self.getNode().getPosition())
            #bulletp = bullet.getNode().convertLocalToWorldPosition(bullet.getNode().getPosition())
            #print "target: " + str(target.x) + ", " + str(target.y) + ", " + str(target.z)
            #print "weapon: " + str(weaponp.x) + ", " + str(weaponp.y) + ", " + str(weaponp.z)
            #print "bullet: " + str(bulletp.x) + ", " + str(bulletp.y) + ", " + str(bulletp.z)
            #cw = self.getNode().convertWorldToLocalPosition(target)
            #print "gdzies tu"
            phys = self.levelFactory.getLevel().getWorld()
            bulletShape = phys.buildShape(bullet.getEntity(), "sphere")
            body = phys.buildRigidBody(bullet.getNode(), bulletShape, bullet.mass)
            body.setCcdMotionThreshold(0.2)
            body.setCcdSweptSphereRadius(0.001)
            bd = CentralForceBodyDriver(body, self.getLevelFactory().getLevel().getTimeEventManager())
            #print "target: " + str(target.x) + ", " + str(target.y) + ", " + str(target.z)
            target.normalise()
            #print "target: " + str(target.x) + ", " + str(target.y) + ", " + str(target.z)
            bd.induceMovement(target)#*Ogre.Vector3(-1,-1,1))
            bullet.setBody(body)
            self.levelFactory.registerActorsBody(bullet.getNode(), body)
            self.getTimeEventManager().registerCallbackIn( game.TimeEventManager.RefTEListener(B_308Winchester.onDeath, bullet), 0.4 )
            # stworz pocisk i go poslij
            bullet.driver = bd
            
             
class B_308Winchester(Bullet):
    name = ".308 Winchester"
    damage = 0.5
    mass = 0.1 # kg
    
    def __init__(self, xml, parent, sceneFactory, levelFactory, parser):
        #print "to jest init"
        parentEntity = parent
        Bullet.counter = Bullet.counter + 1
        #print "makenode"
        obj = parser.elementFactory.makeNode(self.name+str(Bullet.counter), parentEntity)
        #print "ra"
        trace = parser.elementFactory.makeParticleSystem("Trace"+str(obj), "Trace")
        levelFactory.registerSceneActor(obj, self)
        self.node = obj
        self.levelFactory = levelFactory
        self.parser = parser
        self.sceneFactory = sceneFactory
        self.xml = xml
        self.entity = self.parser.elementFactory.createEntity(Ogre.SceneManager.PT_SPHERE)
        self.node.attachObject(self.entity)
        self.node.attachObject(trace)
        self.trace = trace
        #self.node.setPosition(location)
        self.node.setScale(0.003, 0.003, 0.003)
        
    def onDeath(self):
        self.trace.clear()
        self.trace.setEmitting(False)
        self.driver.destroyAll()
        self.getLevelFactory().removeActor(self)
        print "TRACE", self.trace.isInScene()
    
    
