from game.Level import Character, Mortal, WeaponSelection, Actor, Ghost,Movable
import game.BodyDriver
import Weapons
import ogre.gui.CEGUI as CEGUI
from game.AIControl import StraightToEnemy, Shooter
from game.DotSceneParser import DotSceneParser
from game.SceneIOListener import SecondaryIOListener
from game.TerrainClickLocator import TerrainClickListener
import ogre.io.OIS as OIS
import ogre.renderer.OGRE as Ogre
from game.CooldownDecorator import CooldownWard
from game.PCSkills import PCSkills

"""
class OgreMotionState(bullet.btMotionState): 
    def __init__(self, initialPosition, node): 
        bullet.btMotionState.__init__(self)
        self.Pos = initialPosition
        self.node = node
        
    def getWorldTransform(self, WorldTrans): 
        WorldTrans.setOrigin(self.Pos.getOrigin())
        WorldTrans.setBasis(self.Pos.getBasis())
          
    def setWorldTransform(self, worldTrans):
        rot = worldTrans.getRotation();
        self.node.setOrientation(rot.w(), rot.x(), rot.y(), rot.z());
        pos = worldTrans.getOrigin();
        self.node.setPosition(pos.x(), pos.y(), pos.z());
"""



class LevelFinishGhost(Ghost):
    def collisionEvent(self, actor2):
        if hasattr(actor2, '_pc_marker'):
            self.getLevel().finish(message="[colour='FF00FF00']GREAT SUCCESS!")

class FirstEnemyBall(Character, Movable, Mortal):
    def onLevelLoaded(self):
        print "FirstEnemyBall, ONLOADED"
        print self.getNode().getName()
        mDriver = game.BodyDriver.CentralForceBodyDriver(self.getBody(), 
                                                         self.getLevelFactory().level.getTimeEventManager())
        self.setDriver(mDriver)
        enemyName = self.getLevelFactory().getLevel().getSettings()['playerActorName']
        enemyNode = self.getLevelFactory().getLevel().actorForName(enemyName).getNode()
        mAIManager = self.getLevelFactory().getAIManager()
        self.ai_strategy = StraightToEnemy(self, enemyNode)
        mAIManager.registerStrategy(self.ai_strategy)
    
    @CooldownWard(0.1)
    def meleeHit(self, enemy):
        enemy.onDamageAbsorbed(1)
    
    def collisionEvent(self, actor2):
        if hasattr(actor2, '_pc_marker'):
            self.meleeHit(actor2)

    def onDeath(self):
        self.getLevel().getSoundManager().addSoundToNode(self.getNode(), "growl.wav", loop=False)
        Mortal.onDeath(self)
        
        mAIManager = self.getLevelFactory().getAIManager()
        mAIManager.removeStrategy(self.ai_strategy)
        del self.ai_strategy

class EvilFloor(Actor):
    @CooldownWard(0.1)
    def collisionEvent(self, actor):
        if hasattr(actor, '_pc_marker'):
            actor.onDamageAbsorbed(10)

class VeryEvilFloor(Actor):
    @CooldownWard(0.1)
    def collisionEvent(self, actor):
        if hasattr(actor, '_pc_marker'):
            actor.onDamageAbsorbed(20)
        

class HeartBallAgg(Character, Movable, Mortal):
    def onLevelLoaded(self):
        print "HeartBall, ONLOADED"
        print self.getNode().getName()
        mDriver = game.BodyDriver.CentralForceBodyDriver(self.getBody(), 
                                                         self.getLevelFactory().level.getTimeEventManager())
        self.setDriver(mDriver)
        enemyName = self.getLevelFactory().getLevel().getSettings()['playerActorName']
        enemyNode = self.getLevelFactory().getLevel().actorForName(enemyName).getNode()
        mAIManager = self.getLevelFactory().getAIManager()
        
        def force(e, i):
            return (e - i).normalisedCopy()*(e-i).length()*(e-i).length()
        
        self.ai_strategy = StraightToEnemy(self, enemyNode, force = force)
        mAIManager.registerStrategy(self.ai_strategy)
    
    def collisionEvent(self, actor2):
        pass

    def onDeath(self):
        Mortal.onDeath(self)
        mAIManager = self.getLevelFactory().getAIManager()
        mAIManager.removeStrategy(self.ai_strategy)
        del self.ai_strategy
        
class HeartBall(Character, Movable, Mortal):
    def onLevelLoaded(self):
        print "HeartBall, ONLOADED"
        print self.getNode().getName()
        mDriver = game.BodyDriver.CentralForceBodyDriver(self.getBody(), 
                                                         self.getLevelFactory().level.getTimeEventManager())
        self.setDriver(mDriver)
        enemyName = self.getLevelFactory().getLevel().getSettings()['playerActorName']
        enemyNode = self.getLevelFactory().getLevel().actorForName(enemyName).getNode()
        mAIManager = self.getLevelFactory().getAIManager()
        
        def force(e, i):
            if (e-i).length() <= 10.0:
                return (e - i).normalisedCopy()*0.0
            return (e - i).normalisedCopy()*(e-i).length()
        
        self.ai_strategy = StraightToEnemy(self, enemyNode, force = force)
        mAIManager.registerStrategy(self.ai_strategy)
    
    def collisionEvent(self, actor2):
        pass

    def onDeath(self):
        Mortal.onDeath(self)
        mAIManager = self.getLevelFactory().getAIManager()
        mAIManager.removeStrategy(self.ai_strategy)
        del self.ai_strategy

class ImmortalCannon(Actor):
    def onLevelLoaded(self):
        self.currentHP = 1000
        self.maxHP = 1000
        self.weapons = WeaponSelection()
        self.weapons.addWeapon(Weapons.Minigun(None, self.getNode(), self.sceneFactory, self.levelFactory, self.parser))
        self.weapons.getCurrent().ammo = 1000
        enemyName = self.getLevelFactory().getLevel().getSettings()['playerActorName']
        #print "Cannon, trololo: " + enemyName
        enemyNode = self.getLevelFactory().getLevel().actorForName(enemyName).getNode()
        mAIManager = self.getLevelFactory().getAIManager()
        shooter = Shooter(self)
        shooter.addTarget(enemyNode)
        mAIManager.registerStrategy(shooter)   
        
class Cannon(Mortal):
    def onLevelLoaded(self):
        self.currentHP = 1000
        self.maxHP = 1000
        self.weapons = WeaponSelection()
        self.weapons.addWeapon(Weapons.Minigun(None, self.getNode(), self.sceneFactory, self.levelFactory, self.parser))
        self.weapons.getCurrent().ammo = 1000
        enemyName = self.getLevelFactory().getLevel().getSettings()['playerActorName']
        #print "Cannon, trololo: " + enemyName
        enemyNode = self.getLevelFactory().getLevel().actorForName(enemyName).getNode()
        mAIManager = self.getLevelFactory().getAIManager()
        shooter = Shooter(self)
        shooter.addTarget(enemyNode)
        mAIManager.registerStrategy(shooter)        

class GreatCannon(Mortal):
    def onLevelLoaded(self):
        self.currentHP = 1000
        self.maxHP = 1000
        self.weapons = WeaponSelection()
        self.weapons.addWeapon(Weapons.BallLauncher(None, self.getNode(), self.sceneFactory, self.levelFactory, self.parser))
        self.weapons.getCurrent().ammo = 1000
        enemyName = self.getLevelFactory().getLevel().getSettings()['playerActorName']
        #print "Cannon, trololo: " + enemyName
        enemyNode = self.getLevelFactory().getLevel().actorForName(enemyName).getNode()
        mAIManager = self.getLevelFactory().getAIManager()
        shooter = Shooter(self, 100)
        shooter.addTarget(enemyNode)
        mAIManager.registerStrategy(shooter)  

class GhostActivator(Ghost):
    def collisionEvent(self, actor2):
        pass

class HeroBall(Character, Mortal, Movable, SecondaryIOListener, PCSkills):
    _pc_marker = True
    
    def onLevelLoaded(self):
        self.getDriver().registerOnJumpListener(self.getLevel().getGUI())
    
    def getEnergy(self):
        return self.currentEnergy
    
    def modEnergy(self, amount):
        self.currentEnergy += amount
        if self.currentEnergy < 0:
            self.currentEnergy = 0
        if self.currentEnergy > self.getMaxEnergy():
            self.currentEnergy= self.getMaxEnergy()
        self.getLevel().getGUI().setEnergy(self.currentEnergy)
        
    def getMaxEnergy(self):
        return 100
    
    def onDamageAbsorbed(self, *args, **kwargs):
        Mortal.onDamageAbsorbed(self, *args, **kwargs)
        self.getLevel().getGUI().setHealth(self.getHP())
    
    def mousePressed(self, evt, bid):
        pass
    def onMove(self, vector3d):
        target = self.getNode().convertWorldToLocalPosition(vector3d)
        self.getNode().setDirection(target.x, target.y, target.z)
    def onClick(self, vector3d):
        print "onclick"
        self.weapons.getCurrent().activate(vector3d)
        print "ocexit"
        
    def isActive(self):
        return True
    def __init__(self, xml, parent, sceneFactory, levelFactory, parser):
        Actor.__init__( self, xml, parent, sceneFactory, levelFactory, parser )
        print "HeroBall init"
        listener = levelFactory.level.getListener()
        listener.registerSecondary(self)
        self.weapons = WeaponSelection()
        self.currentHP = 100
        self.currentEnergy = 100
        self.maxHP = 100
        levelFactory.mTerrainClickLocator.registerListener(self)
        self.weapons.addWeapon(Weapons.Minigun(xml, self.getNode(), sceneFactory, levelFactory, parser))
        self.weapons.getCurrent().ammo = 200
        self.setDriver(game.BodyDriver.CentralForceBodyDriver(self.getBody(), 
                                                              levelFactory.level.getTimeEventManager()))
        
        #self.getNode().setScale(0.5,0.5,1)
    
    
    # zakomentuj onDeath, by wycrashowac pythona po smierci gracza. game over. bwahahha.
    def onDeath(self):
        self.getLevel().finish(message="[colour='FFFF0000']YOU GOT PWNED!")
    
    
class Hero(Character):
    def __init__(self, xml, parent, sceneFactory, levelFactory, parser):
        print "building a hero"
        empty = parser.reallyParseNode(xml, parent)
        #  def __init__(self, name, resourceGroup, sceneMgr, physWrapper, sceneElementFactory, levelElementFactory, log):
        
        subParser = DotSceneParser("Hero.scene", 
                                   "SceneCommons", 
                                   parser.sceneMgr, 
                                   parser.physWrapper, 
                                   sceneFactory, 
                                   levelFactory, 
                                   parser.log, 
                                   empty)
        subParser.build()
        
        """node = xml
        parentEntity = parent
        obj = parser.elementFactory.makeNode(node.getAttribute("name"), parentEntity)
        levelFactory.registerSceneActor(obj, self)
        for x in node.childNodes:
            if x.nodeName == 'position':
                parser.setPosition(x, obj)
            #parser.nodeChildDispatch.get(x.nodeName, parser.handlerNotFound)(x, obj)    
        self.node = obj
        
        
        # Budujemy entity
        entity = sceneFactory.makeEntity('scenecommons-hero-mesh-mg',
                                            'Hero.mesh',
                                            'SceneCommons')
        obj.attachObject(entity)
        #     body = self.physWrapper.buildRigidBody(parent, shape, mass)
        """
import TimeEventManager

class AnimTest(Character, TimeEventManager.TimeEventListener):
     def __init__(self, xml, parent, sceneFactory, levelFactory, parser):
        node = xml
        parentEntity = parent
        obj = parser.elementFactory.makeNode(node.getAttribute("name"), parentEntity)
        levelFactory.registerSceneActor(obj, self)
        for x in node.childNodes:
            if x.nodeName == 'position':
                parser.setPosition(x, obj)
            #parser.nodeChildDispatch.get(x.nodeName, parser.handlerNotFound)(x, obj)    
        self.node = obj
        
        """
                robot = self.factory.sceneManager.createEntity("robot", "robot.mesh", "SceneCommons")
        states = robot.getAllAnimationStates()
        print robot, states
        for x in states.getAnimationStateIterator():
            print x, x.getAnimationName()
        """
        # Budujemy entity
        entity = sceneFactory.makeEntity('animtestmesh',
                                            'Cone.001.mesh',
                                            'SampleScene')
        states = entity.getAllAnimationStates()
        print entity, states
        for x in states.getAnimationStateIterator():
            print x, x.getAnimationName()
        obj.attachObject(entity)     
        
        self.animationState = entity.getAnimationState('Act: ArmatureAction')
        self.animationState.setLoop(True)
        self.animationState.setEnabled(True)
    
        levelFactory.level.timeEventManager.registerOnNewFrame(self)
     
     def onTimeEvent(self, arg, dummy):
         self.animationState.addTime(arg)

class ParticleTest(Character):
     def __init__(self, xml, parent, sceneFactory, levelFactory, parser):
        node = xml
        parentEntity = parent
        obj = parser.elementFactory.makeNode(node.getAttribute("name"), parentEntity)
        levelFactory.registerSceneActor(obj, self)
        for x in node.childNodes:
            if x.nodeName == 'position':
                parser.setPosition(x, obj)
            #parser.nodeChildDispatch.get(x.nodeName, parser.handlerNotFound)(x, obj)    
        self.node = obj
        
        """
                robot = self.factory.sceneManager.createEntity("robot", "robot.mesh", "SceneCommons")
        states = robot.getAllAnimationStates()
        print robot, states
        for x in states.getAnimationStateIterator():
            print x, x.getAnimationName()
        """
        # Budujemy entity
        entity = sceneFactory.makeParticleSystem('fountain_object_ps',
                                            'fountain')
        
        obj.attachObject(entity)
     
"""
   def addEntity(self, node, parent):
        obj = self.elementFactory.makeEntity(node.getAttribute("meshFile") + "_ent_" + self.resourceGroup,
                                            node.getAttribute("meshFile"),
                                            self.resourceGroup)
        body = None
        mass = float(node.getAttribute("mass") or "0.0")
        if node.hasAttribute("collisionPrim"):
            shape = self.physWrapper.buildShape(obj, node.getAttribute("collisionPrim"))
        
        if node.getAttribute("physics_type") == "NO_COLLISION" or not node.hasAttribute("physics_type"):
            pass
        elif node.getAttribute("physics_type") == "STATIC":
            body = self.physWrapper.buildStaticBody(parent, shape)
        elif node.getAttribute("physics_type") == "RIGID_BODY":
            body = self.physWrapper.buildRigidBody(parent, shape, mass)
        else:
            self.log.logMessage("Unrecognised physics_type: %s" % node.getAttribute("physics_type"), Ogre.LML_NORMAL)
    
        if node.getAttribute("actor") == "True":
            self.levelElementFactory.registerActorsBody(parent, body)
        parent.attachObject(obj);
"""

"""
            "position": self.setPosition,
            "rotation": self.setRotation,
            "scale": self.setScale,
            "node": self.parseNode,
            "user_data": self.parseUserData,
            "entity": self.addEntity,
            "light": self.addLight,
"""

"""
        <node name="Hero" >
            <position y="14.724826" x="5.500507" z="-3.044533" />
            <rotation qy="1.000000" qx="0.000000" qz="-0.000000" qw="0.000000" />
            <scale y="2.196033" x="2.196033" z="2.196033" />
            <game >
                <sensors />
                <actuators />
            </game>
            <entity anisotropic_friction="False" lock_trans_y="False" damping_trans="0.03999999910593033" damping_rot="0.10000000149011612" inertia_tensor="0.4000000059604645" actor="True" velocity_min="0.0" lock_trans_z="False" physics_type="RIGID_BODY" lock_trans_x="False" meshFile="Sphere.mesh" friction_y="1.0" friction_x="1.0" friction_z="1.0" velocity_max="0.0" ghost="False" name="Sphere" mass_radius="1.0" mass="1.0" collisionPrim="sphere" lock_rot_x="False" lock_rot_y="False" lock_rot_z="False" />
        </node>


    def __init__(self, xml, parent, sceneFactory, levelFactory, parser):
        node = xml
        parentEntity = parent
        obj = parser.elementFactory.makeNode(node.getAttribute("name"), parentEntity)
        levelFactory.registerSceneActor(obj, self)
        for x in node.childNodes:
            parser.nodeChildDispatch.get(x.nodeName, parser.handlerNotFound)(x, obj)    
        self.node = obj
"""


