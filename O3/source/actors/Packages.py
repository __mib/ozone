from game.Level import Character, Mortal, WeaponSelection, Actor, Ghost,Movable
from game.CooldownDecorator import CooldownWard

class Energy(Mortal):
    @CooldownWard(0.5)
    def collisionEvent(self, actor2):        
        if hasattr(actor2, '_pc_marker'):
            actor2.modEnergy(25)
            self.onDeath()

class Health(Mortal):
    @CooldownWard(0.5)
    def collisionEvent(self, actor2):        
        if hasattr(actor2, '_pc_marker'):
            actor2.onDamageAbsorbed(-25)
            self.onDeath()