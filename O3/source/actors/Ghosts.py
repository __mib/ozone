from game.Level import Character, Mortal, WeaponSelection, Actor, Ghost,Movable
from game.CooldownDecorator import CooldownWard


class SPC(Ghost):
    @CooldownWard(0.5)
    def collisionEvent(self, actor2):
        try:        
            slider = self.getLevelFactory().getSlider(actor2.getBody())
            slider.setTargetLinMotorVelocity(-slider.getTargetLinMotorVelocity())
        except KeyError:
            pass
    
class Floor(Actor):
    pass

class Teleport(Actor):
    @CooldownWard(10.0)
    def collisionEvent(self, actor2):
        print "Collision"
        actor2.getLevel().getWorld().teleport(actor2)        
        
        